#!/usr/bin/python
#
# Script Python de lancement de la chaine de recalage raster/vecteur
# enchaine les etapes suivantes :
# - Transformation de la base de données dans la géométrie de l'image
# - Cropping de l'image d'entrée autour de la BD
# - Calcul LSD sur l'image croppée
# - Filtrage du LSD par taille, perpendicularité et proximité autour de la BD, puis calcul de la trasformation pour effectuer le recalage
# - Calcul de differents score : fractions de segments apparies, score par polygone de la BD

import sys
import glob
import otbApplication
import os
import logging
import argparse
import subprocess

# environment
maxram=8192

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main(argv):

    parser = argparse.ArgumentParser()
    # Mandatory arguments
    parser.add_argument("im",   help="Input image")
    parser.add_argument("db",   help="Input db")
    parser.add_argument("wd",   help="Working directory")

    # Optional arguments
    # CropAroundDB arguments
    parser.add_argument("-margin",      type=float,    help="Margin applied on the global vector database's envelope (default : 10.)")
    # FLSD arguments
    parser.add_argument("-flsd",        type=str2bool, help="Use fast lsd method (if not set, default lsd process will be used)", default=False)
    parser.add_argument("-scale",       type=float,    help="Gaussian scaling (default : 0.8)")
    parser.add_argument("-sigmacoef",   type=float,    help="Ratio for sigma of gaussian filter (default : 0.6)")
    parser.add_argument("-quant",       type=float,    help="Gradient quantization error (default : 2.)")
    parser.add_argument("-angth",       type=float,    help="Gradient angle tolerance (default : 22.5)")
    parser.add_argument("-logeps",      type=float,    help="Detection threshold (default : 0.)")
    parser.add_argument("-densityth",   type=float,    help="Minimal density of region points in a rectangle to be accepted (default : 0.7)")
    parser.add_argument("-nbins",       type=int,      help="Number of bins in ordering (default : 1024)")
    parser.add_argument("-tilesizex",   type=int,      help="Size of tiles in pixel along X-axis (default : 500)")
    parser.add_argument("-tilesizey",   type=int,      help="Size of tiles in pixel along Y-axis (default : 500)")
    # Registration arguments
    parser.add_argument("-minlenth",    type=float,    help="Minimum threshold for length filter (default : 5.)")
    parser.add_argument("-maxlenth",    type=float,    help="Maximum threshold for length filter (default : 100.)")
    parser.add_argument("-maxdistperp", type=float,    help="Max distance allowed between 2 perpendicular segments (default : 10.)")
    parser.add_argument("-angletol",    type=float,    help="Angle tolerance (in degree) for perpendicular filter (default : 15.)")
    parser.add_argument("-maxdistdb",   type=float,    help="Maximum distance to any geometry of the vector database allowed (default : 10.)")
    # Evaluation arguments
    parser.add_argument("-matchth",     type=float,    help="Match threshold (default : 2.0)")
    parser.add_argument("-rateth",      type=float,    help="Rate threshold (default : 0.3)")
    # Global arguments
    parser.add_argument("-clean",       type=str2bool, help="Clean intermediate outputs (default : True)", default=True)

    args = parser.parse_args()

    image = args.im
    input_db = args.db
    working_dir = args.wd
    
    # Working directory
    name, ext_img = os.path.splitext(os.path.basename(image))
    name, ext_db = os.path.splitext(os.path.basename(input_db))
    print ("Working directory : ", working_dir)
    if os.path.exists(working_dir):
        print (working_dir," already exists")
    else:
        try:
            subprocess.call(["mkdir","-p", working_dir])
            print ("Creating ", working_dir)
        except:
            print("Error in creating working directory")
            exit(-1)
 
    output_image = os.path.join(working_dir, "base_image.TIF")
    try:
        subprocess.call(["cp", image, output_image])
        print("Copying input image")
        print(image.replace(ext_img, ".geom"))
        if os.path.exists(image.replace(ext_img, ".geom")):
            print("Copying input geometry file")
            subprocess.call(["cp", image.replace(ext_img, ".geom"), output_image.replace(".TIF",".geom")])
        image = output_image
    except:
        print("Error in copying file")
        exit(-1)
 
    print()
    print("Reproject database into image geometry")
    print("--------------------------------------")
    proj_db = os.path.join(working_dir,"database_reprojected" + ext_db)
    app = otbApplication.Registry.CreateApplication("VectorDataReprojection")
    app.SetParameterString("in.vd", input_db)
    app.SetParameterString("out.proj", "image")
    app.SetParameterString("out.proj.image.in", image)
    app.SetParameterString("out.vd", proj_db)
    try:
        app.ExecuteAndWriteOutput()
    except:
        print("Error in VectorDataReprojection")
        exit(-1)
    
    print()
    print("Crop input image around DB")
    print("--------------------------")
    crop_im = os.path.join(working_dir,"cropped_image.TIF")
    app = otbApplication.Registry.CreateApplication("CropAroundDB")
    app.SetParameterString("im", image)
    app.SetParameterString("db", proj_db)
    app.SetParameterString("out", crop_im)
    if (args.margin) :
        print("Margin is set")
        app.SetParameterFloat("margin", args.margin)
    try:
        app.ExecuteAndWriteOutput()
    except:
        print("Error in CropAroundDB")
        exit(-1)
        
    print()
    print("Line Segment Detection")
    print("----------------------")
    lsd = os.path.join(working_dir,"lsd_default" + ext_db)
    if (args.flsd) :
        lsd = os.path.join(working_dir,"lsd_fast" + ext_db)
        app = otbApplication.Registry.CreateApplication("FastLineSegmentDetection")
        app.SetParameterString("in", crop_im)
        app.SetParameterString("lsd", lsd)
        if (args.scale) :
            app.SetParameterFloat("scale", args.scale)
        if (args.sigmacoef) :
            app.SetParameterFloat("sigmacoef", args.sigmacoef)
        if (args.quant) :
            app.SetParameterFloat("quant", args.quant)
        if (args.angth) :
            app.SetParameterFloat("angth", args.angth)
        if (args.logeps) :
            app.SetParameterFloat("logeps", args.logeps)
        if (args.densityth) :
            app.SetParameterFloat("densityth", args.densityth)
        if (args.nbins) :
            app.SetParameterInt("nbins", args.nbins)
        if (args.tilesizex) :
            app.SetParameterInt("tilesizex", args.tilesizex)
        if (args.tilesizey) :
            app.SetParameterInt("tilesizey", args.tilesizey)
        
    else:
        app = otbApplication.Registry.CreateApplication("LineSegmentDetection")
        app.SetParameterString("in", crop_im)
        app.SetParameterString("out", lsd)
    try:
        app.ExecuteAndWriteOutput()
    except:
        print("Error in LSD")
        exit(-1)

    print()
    print("Segment Filtering and Registration")
    print("----------------------------------")
    regis_db = "database_registered" + ext_db
    app = otbApplication.Registry.CreateApplication("LSDToDBRegistration")
    app.SetParameterString("lsd", lsd)
    app.SetParameterString("db", proj_db)
    app.SetParameterString("out", regis_db)
    app.SetParameterString("outputdir", working_dir)
    app.SetParameterInt("debug", 1)
    if (args.minlenth) :
        app.SetParameterFloat("minlenth", args.minlenth)
    if (args.maxlenth) :
        app.SetParameterFloat("maxlenth", args.maxlenth)
    if (args.maxdistperp) :
        app.SetParameterFloat("maxdistperp", args.maxdistperp)
    if (args.angletol) :
        app.SetParameterFloat("angletol", args.angletol)
    if (args.maxdistdb) :
        app.SetParameterFloat("maxdistdb", args.maxdistdb)
    try:
        app.ExecuteAndWriteOutput()
    except:
        print("Error in LSDToDBRegistration")
        exit(-1)
    regis_db = os.path.join(working_dir, regis_db)

    print()
    print("Evaluation")
    print("----------")
    oustats = os.path.join(working_dir,"globalstats.xml")
    app = otbApplication.Registry.CreateApplication("VectorDataEvaluation")
    app.SetParameterString("in", lsd)
    app.SetParameterString("ref", regis_db)
    app.SetParameterString("outstats", oustats)
    if (args.matchth) :
        app.SetParameterFloat("matchthreshold", args.matchth)
    if (args.rateth) :
        app.SetParameterFloat("ratethreshold", args.rateth)
    try:
        app.ExecuteAndWriteOutput()
    except:
        print("Error in VectorDataEvaluation")
        exit(-1)
        
    if(args.clean):
        print()
        print("Clean intermediate outputs")
        print("--------------------------")
        subprocess.call(["rm", crop_im])
        if os.path.exists(crop_im.replace(ext_img, ".geom")):
            subprocess.call("rm " + crop_im.replace(ext_img, ".geom"), shell=True)
        subprocess.call("rm " + os.path.join(working_dir, "db_intersectionPoints.*"), shell=True)
        subprocess.call("rm " + os.path.join(working_dir, "db_perpendicularFiltering.*"), shell=True)
        subprocess.call("rm " + lsd.replace(ext_db, ".*"), shell=True)
        subprocess.call("rm " + os.path.join(working_dir, "lsd_intersectionPoints.*"), shell=True)
        subprocess.call("rm " + os.path.join(working_dir, "lsd_lengthFilter.*"), shell=True)
        subprocess.call("rm " + os.path.join(working_dir, "lsd_perpendicularityFilter.*"), shell=True)
        print("Working directory has been cleaned")

if __name__ == "__main__":
    main(sys.argv)
