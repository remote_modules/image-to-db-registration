/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbVectorData.h"

#include "otbImageFileReader.h"
#include "otbVectorImageToIntensityImageFilter.h"
#include "otbLineSegmentDetector.h"

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbLSDToDBRegistrationProcessFilter.h"

namespace otb
{

//  LSDToDBRegistration class is defined in Wrapper namespace.
namespace Wrapper
{

//  LSDToDBRegistration class is derived from Application class.

class LSDToDBRegistration : public Application
{
private:
  using PixelType              = double;
  using SingleImageType        = otb::Image<PixelType, 2>;
  using RegistrationFilterType = otb::LSDToDBRegistrationProcessFilter<PixelType>;

public:
  // Class declaration is followed by \code{ITK} public types for the class, the superclass and
  // smart pointers.

  using Self         = LSDToDBRegistration;
  using Superclass   = Application;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  //  Following macros are necessary to respect ITK object factory mechanisms. Please report
  //  to \ref{sec:FilterConventions} for additional information.

  itkNewMacro(Self);
  itkTypeMacro(LSDToDBRegistration, otb::Application);

private:
  void DoInit() override
  {
    SetName("LSDToDBRegistration");
    SetDescription("This application computes a transformation between an image and a vector database, and apply it on the vector DB.");
    SetDocLongDescription("This application computes a transformation between an image and a vector database, and apply it on the vector DB."
            "It uses the OTB Line Segment Detector (LSD) algorithm to detect segments in the input image.");
    SetDocLimitations("None");
    SetDocAuthors("Germain SALGUES, Rémi DEMORTIER, Yannick TANGUY");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::Filter);

    // Input data
    AddParameter(ParameterType_InputFilename, "db", "Input vector database path");
    SetParameterDescription("db", "Input vector database path.");
    AddParameter(ParameterType_InputFilename, "lsd", "LSD file path");
    SetParameterDescription("lsd", "Input LSD file path.");

    // Output and working directories
    AddParameter(ParameterType_Directory, "outputdir", "Output directory");
    SetParameterDescription("outputdir", "Output directory.");
    AddParameter(ParameterType_OutputFilename, "out", "Transformed DB file name");
    SetParameterDescription("out", "Transformed DB file.");
    AddParameter(ParameterType_Bool, "debug", "Debug mode");
    SetParameterDescription("debug", "By specifying debug you can enable intermediate OGR vector files generation (for debuging purpose only)");
    MandatoryOff("debug");

    // Length filtering
    AddParameter(ParameterType_Float, "minlenth", "Min length threshold");
    SetParameterDescription("minlenth", "Minimum threshold for length filter.");
    SetDefaultParameterFloat("minlenth", 5.);
    MandatoryOff("minlenth");
    AddParameter(ParameterType_Float, "maxlenth", "Max length threshold");
    SetParameterDescription("maxlenth", "Maximum threshold for length filter.");
    SetDefaultParameterFloat("maxlenth", 100.);
    MandatoryOff("maxlenth");
    // Perpendicular filtering
    AddParameter(ParameterType_Float, "maxdistperp", "Max distance allowed between 2 perpendicular segments");
    SetParameterDescription("maxdistperp", "Maximum shortest distance for perpendicular filter.");
    SetDefaultParameterFloat("maxdistperp", 10.);
    MandatoryOff("maxdistperp");
    AddParameter(ParameterType_Float, "angletol", "Angle tolerance (in degree)");
    SetParameterDescription("angletol", "Angle tolerance (in degree) for perpendicular filter.");
    SetDefaultParameterFloat("angletol", 15.);
    MandatoryOff("angletol");
    // Proximity filtering
    AddParameter(ParameterType_Float, "maxdistdb", "Max distance to any geometry of the vector database allowed");
    SetParameterDescription("maxdistdb", "Maximum distance to any geometry of the vector database allowed.");
    SetDefaultParameterFloat("maxdistdb", 10.);
    MandatoryOff("maxdistdb");

    // Doc example parameter settings
    SetDocExampleParameterValue("lsd", "lsd.shp");
    SetDocExampleParameterValue("db", "input_vectorDB.shp");
    SetDocExampleParameterValue("out", "output_vectorDB.shp");
    SetDocExampleParameterValue("outputdir", "/path/to/outdir");
    SetDocExampleParameterValue("debug", "1");
    SetDocExampleParameterValue("minlenth", "5.");
    SetDocExampleParameterValue("maxlenth", "100.");
    SetDocExampleParameterValue("maxdistperp", "10.");
    SetDocExampleParameterValue("angletol", "15.");
    SetDocExampleParameterValue("maxdistdb", "10.");
  }

  void DoUpdateParameters() override {}

  void DoExecute() override
  {
    RegistrationFilterType::Pointer registrationFilter = RegistrationFilterType::New();

    registrationFilter->SetInputVectorDBName(GetParameterString("db"));
    registrationFilter->SetInputLSDFileName(GetParameterString("lsd"));
    registrationFilter->SetOutputDir(GetParameterString("outputdir"));
    registrationFilter->SetOutputVectorDBName(GetParameterString("out"));
    if(IsParameterEnabled("debug")){
      otbAppLogINFO("The application will generate intermediate data (OGR vector files)");
      registrationFilter->SetWorkingDir(GetParameterString("outputdir"));
    }
    registrationFilter->SetMinLengthThreshold(GetParameterFloat("minlenth"));
    registrationFilter->SetMaxLengthThreshold(GetParameterFloat("maxlenth"));
    registrationFilter->SetMaxDistancePerpendicularity(GetParameterFloat("maxdistperp"));
    registrationFilter->SetAngleToleranceDeg(GetParameterFloat("angletol"));
    registrationFilter->SetMaxDistanceToVectorDB(GetParameterFloat("maxdistdb"));

    registrationFilter->Process();
  }
};

} // namespace Wrapper
} // namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::LSDToDBRegistration)
