/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbOGRDataSourceWrapper.h"
#include "otbOGRFeatureWrapper.h"
#include "otbStatisticsXMLFileWriter.h"
#include "itkVariableLengthVector.h"
#include "otbOGRIOHelper.h"
#include "otbVectorData.h"

#include "otbVectorDataHelper.h"
#include "otbOGRHelper.h"


namespace otb
{

namespace Wrapper
{

class VectorDataEvaluation : public Application
{
public:
  /** Standard class typedefs. */
  typedef VectorDataEvaluation                                Self;
  typedef Application                                         Superclass;
  typedef itk::SmartPointer<Self>                             Pointer;
  typedef itk::SmartPointer<const Self>                       ConstPointer;

  /** VectorData TypeDef*/
  typedef VectorData<double>                                  VectorDataType;
  typedef VectorDataType::Pointer                             VectorDataPointerType;
  typedef VectorDataType::DataNodeType                        DataNodeType;
  typedef DataNodeType::Pointer                               DataNodePointerType;
  typedef typename VectorDataType::DataTreeType::TreeNodeType InternalTreeNodeType;

  typedef VectorDataType::LineType                            LineType;
  typedef VectorDataType::PointType                           PointType;
  typedef LineType::VertexType                                VertexType;
  typedef LineType::VertexListType                            VertexListType;
  typedef otb::VectorDataFileWriter<VectorDataType>           VectoDataWriterType;

  // VectorData Tools
  typedef otb::VectorDataHelper<VectorDataType>               VectorDataHelperType;
  typedef std::vector<VertexType>                LineVectorType;
  typedef std::vector<LineVectorType>            MultiLineVectorType;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(VectorDataEvaluation, otb::Application);

private:
  void DoInit() override
  {
    SetName("VectorDataEvaluation");
    SetDescription("Compute evaluation metrics between two OGR Layers");

    SetDocLongDescription("Compute evaluation metrics in order to compare two OGR Layers, one being the reference");
    SetDocLimitations("Experimental. For now only shapefiles are supported.");
    SetDocAuthors("Germain SALGUES");

    AddParameter(ParameterType_InputVectorData, "in", "LSD Vector Data");
    SetParameterDescription("in","Name of the input shapefile (lsd from raw image)");

    AddParameter(ParameterType_InputVectorData, "ref", "DataBase Vector Data");
    SetParameterDescription("ref","Name of the reference shapefile (database)");

    AddParameter(ParameterType_OutputFilename, "outstats", "Ouput XML file");
    SetParameterDescription("outstats", "XML file containing mean and variance of each feature.");

    AddParameter(ParameterType_ListView,  "feat", "Feature");
    SetParameterDescription("feat","List of features to consider for statistics.");

    AddParameter(ParameterType_Float,  "matchthreshold", "Match Threshold");
    SetParameterDescription("matchthreshold", "Threshold to determine if two segments match, threshold in meters apply on the distances of the two extremities of the match segment)");
    SetDefaultParameterFloat("matchthreshold", 2.0);

    AddParameter(ParameterType_Float,  "ratethreshold", "Rate threshold");
    SetParameterDescription("ratethreshold", "Threshold on match_rate to determine if a database feature has a match (threshold on the ratio of the feature that has a match)");
    SetDefaultParameterFloat("ratethreshold", 0.3);

    AddParameter(ParameterType_String, "tmpdir", "Path to temporary directory");
    SetParameterDescription("tmpdir", "By specifying tmpdir you can enable intermediate data generation (for debuging purpose only)");
    MandatoryOff("tmpdir");

    SetDocExampleParameterValue("in", "raw_lsd.shp");
    SetDocExampleParameterValue("ref", "registered_database.shp");
    SetDocExampleParameterValue("matchthreshold", "4");
    SetDocExampleParameterValue("ratethreshold", "0.3");
    SetDocExampleParameterValue("outstats", "globalstats.xml");
  }

  void DoUpdateParameters() override
  {
    if ( HasValue("ref") )
    {
      std::string shapefile = GetParameterString("ref");

      otb::ogr::DataSource::Pointer ogrDS;
      otb::ogr::Layer layer(ITK_NULLPTR, false);

      OGRSpatialReference oSRS("");


      ogrDS = otb::ogr::DataSource::New(shapefile, otb::ogr::DataSource::Modes::Read);
      layer = ogrDS->GetLayer(0);

      otb::ogr::Feature feature = layer.ogr().GetNextFeature();
      ClearChoices("feat");

      std::vector<std::string> choiceKeys;

      for(int iField=0; iField<feature.ogr().GetFieldCount(); iField++)
      {
        std::string key, item = feature.ogr().GetFieldDefnRef(iField)->GetNameRef();
        key = item;

        /** Transform chain : lowercase and alphanumerical */
        key.erase(std::remove_if(key.begin(), key.end(), std::not1(std::ptr_fun(::isalnum))), key.end());
        std::transform(key.begin(), key.end(), key.begin(), tolower);
        /** key must be unique */
        choiceKeys = GetChoiceKeys("feat");
        while(choiceKeys.end() != std::find(choiceKeys.begin(), choiceKeys.end(), key) )
        key.append("0");
        key="feat."+key;
        AddChoice(key,item);
      }
    }

  }

  void DoExecute() override
  {
      clock_t tic = clock();

      // Get the tmpdir parameters (debug)
      bool generated_intermetidate_data = false;
      std::string tmpdir;
      if(IsParameterEnabled("tmpdir")) {
        otbAppLogINFO("The application will generate intermediate data");
        tmpdir = GetParameterString("tmpdir");
        generated_intermetidate_data = true;
      }

      std::string input = GetParameterString("in");
      std::string reference = GetParameterString("ref");
      std::string XMLfile = GetParameterString("outstats");
      double match_threshold = GetParameterFloat("matchthreshold");
      double rate_threshold = GetParameterFloat("ratethreshold");

      double margin = match_threshold;

      otb::ogr::DataSource::Pointer input_vectors = otb::ogr::DataSource::New(input, otb::ogr::DataSource::Modes::Read);
      otb::ogr::DataSource::Pointer reference_vectors = otb::ogr::DataSource::New(reference, otb::ogr::DataSource::Modes::Update_LayerUpdate);

      otb::ogr::Layer input_layer = input_vectors->GetLayer(0);
      otb::ogr::Layer reference_layer = reference_vectors->GetLayer(0);
      OGRFeatureDefn &layerDefn = reference_layer.GetLayerDefn();

      // Test if the filed we want to create exist
      std::string field_rate = "match_rate";
      std::string field_score = "match_scor";
      bool field_rate_exist = false;
      bool field_score_exist = false;
      for(int iField=0; iField< layerDefn.GetFieldCount(); iField++) {
        std::string item = layerDefn.GetFieldDefn(iField)->GetNameRef();
        if(item.compare(field_rate) == 0) {
          field_rate_exist = true;
        }
        if(item.compare(field_score) == 0) {
          field_score_exist = true;
        }
      }

      // Start a transaction for up-comming layer operations (only if supported by layer type)
      if (reference_layer.ogr().TestCapability("Transactions")) {
        OGRErr errStart = reference_layer.ogr().StartTransaction();
        if (errStart != OGRERR_NONE) {
          otbAppLogFATAL(<< "Unable to start transaction for OGR layer " << reference_layer.ogr().GetName() << ".");
        }
      }

      // Create stats fields
      if(!field_rate_exist) {
        otbAppLogINFO("Create field match_rate");
        OGRFieldDefn matchRate("match_rate", OFTReal);
        reference_layer.CreateField(matchRate, false);
      }
      if(!field_score_exist) {
        otbAppLogINFO("Create field match_scor");
        OGRFieldDefn matchScore("match_scor", OFTReal);
        reference_layer.CreateField(matchScore, false);
      }
      reference_layer.ogr().ResetReading();
      otb::ogr::Feature feature = reference_layer.ogr().GetNextFeature();

      otb::OGRIOHelper::Pointer OGRConversion = otb::OGRIOHelper::New();

      // Vector to hold the match segments used for evaluation computation
      MultiLineVectorType debugMetricsMatchsVector;

      // Init global registration score
      int total_feature_count = 0;
      int total_feature_match = 0;
      double total_feature_score = 0;

      // NB: We can't iterate with .ogr().GetNextFeature() as SetFeature may invalidate the
      // iterators depending of the underlying drivers (for example with sqlite format)
      // => we use start_at(), i.e. SetNextByIndex()
      for (unsigned int i=0, N=reference_layer.GetFeatureCount(true); i!=N; ++i) {
        feature = *reference_layer.start_at(i);

        OGRGeometry* geometry = feature.ogr().GetGeometryRef();
        MultiLineVectorType refLineVector;

        VectorDataHelperType::ConvertOGRGeometryToLineVector(geometry, refLineVector);

        // Start reading the input layer (raw lsd to find segments in the neighbourhood of the database feature)
        OGRPolygon* polygon = new OGRPolygon();
        OGRHelper::GetEnvelope(geometry, polygon, margin);
        
        input_layer.ogr().SetSpatialFilter(polygon);
        input_layer.ogr().ResetReading();

        otb::ogr::Feature lsd_feature = input_layer.ogr().GetNextFeature();
        MultiLineVectorType lsdLineVector;
        while(lsd_feature.addr() != ITK_NULLPTR) {
          OGRGeometry* lsd_geometry = lsd_feature.ogr().GetGeometryRef();
          VectorDataHelperType::ConvertOGRGeometryToLineVector(lsd_geometry, lsdLineVector);
          lsd_feature = input_layer.ogr().GetNextFeature();
        }

        /** Perform the actual statistic computation for the current datase feature. */
        double final_score = -1.;
        double final_rate = -1.;
        VectorDataHelperType::ComputeDistanceFromSegmentsToSegments(refLineVector, lsdLineVector,
        		match_threshold, debugMetricsMatchsVector, final_score, final_rate);

        otbAppLogINFO( "Feature n°" << feature.GetFID() << ", match_rate = " << final_rate << " with an average distance of " << final_score);

        if(final_rate > rate_threshold) {
          ++total_feature_match;
          total_feature_score += final_score;
        }

        // Update the current feature
        feature.ogr().SetField("match_rate", final_rate);
        feature.ogr().SetField("match_scor", final_score);
        reference_layer.SetFeature(feature);
        ++total_feature_count;
      }

      // Apply change
      if (reference_layer.ogr().TestCapability("Transactions")) {
        const OGRErr err = reference_layer.ogr().CommitTransaction();
        if (err != OGRERR_NONE) {
          itkExceptionMacro(<< "Unable to commit transaction for OGR layer " << reference_layer.ogr().GetName() << ".");
        }
      }
      reference_vectors->SyncToDisk();

      // Create stats file
      typedef itk::VariableLengthVector<double> MeasurementType;
      MeasurementType global_match, match_mean_score;
      global_match.SetSize(1);
      match_mean_score.SetSize(1);

      global_match = 0;
      match_mean_score = -1;
      if (total_feature_count) {
        global_match = (double) total_feature_match/total_feature_count ;
      }
      match_mean_score = -1;
      if(total_feature_match) {
        match_mean_score = total_feature_score/total_feature_match;
      }

      // The global match is the ratio of database features that pass the individual match threshold.
      // The score is the average of the individual score for database features that pass the individual match threshold.
      otbAppLogINFO(<< "Global match score = " << global_match << " with an average distance of " << match_mean_score);

      typedef otb::StatisticsXMLFileWriter<MeasurementType> StatisticsWriter;
      StatisticsWriter::Pointer writer = StatisticsWriter::New();
      writer->SetFileName(XMLfile);
      writer->AddInput("global_match", global_match);
      writer->AddInput("match_mean_score", match_mean_score);
      writer->Update();

      // dump segments used as support for metrics
      if (generated_intermetidate_data) {
        otbAppLogINFO(<< "Writing the debug metrics support segments to " << tmpdir + std::string("/metrics_support_segments.shp"));
        VectorDataHelperType::WriteVertexVectorToFile(debugMetricsMatchsVector, tmpdir + std::string("/metrics_support_segments.shp"));
      }

      clock_t toc = clock();
      otbAppLogINFO( "Elapsed: "<< ((double)(toc - tic) / CLOCKS_PER_SEC)<<" seconds.");
  }

};
}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::VectorDataEvaluation)
