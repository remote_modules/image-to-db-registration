/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbOGRHelper.h"
#include "otbROIExtractorImageFilter.h"

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

namespace otb
{

//  CropAroundDB class is defined in Wrapper namespace.
namespace Wrapper
{

//  CropAroundDB class is derived from Application class.

class CropAroundDB : public Application
{
private:
  using PixelType              = double;
  using ImageType              = otb::Image<PixelType, 2>;
  using ImageSizeType          = ImageType::SizeType;

  using ROIExtractorFilterType = otb::ROIExtractorImageFilter<ImageType, ImageType>;

public:
  // Class declaration is followed by \code{ITK} public types for the class, the superclass and
  // smart pointers.

  using Self         = CropAroundDB;
  using Superclass   = Application;
  using Pointer      = itk::SmartPointer<Self>;
  using ConstPointer = itk::SmartPointer<const Self>;

  //  Following macros are necessary to respect ITK object factory mechanisms. Please report
  //  to \ref{sec:FilterConventions} for additional information.

  itkNewMacro(Self);
  itkTypeMacro(CropAroundDB, otb::Application);

private:
  void DoInit() override
  {
    SetName("CropAroundDB");
    SetDescription("This application croppes an image around a vector database.");
    SetDocLongDescription("This application croppes an image around a vector database. A margin can also be applied");
    SetDocLimitations("None");
    SetDocAuthors("Rémi DEMORTIER, Yannick TANGUY");
    SetDocSeeAlso(" ");
    AddDocTag(Tags::Filter);

    // Input data
    AddParameter(ParameterType_InputImage, "im", "Input image path");
    SetParameterDescription("im", "Input image path.");
    AddParameter(ParameterType_InputFilename, "db", "Input vector database path");
    SetParameterDescription("db", "Input vector database path.");
    // Output data
    AddParameter(ParameterType_OutputImage, "out", "Output image");
    SetParameterDescription("out", "Output image.");
    // Parameters
    AddParameter(ParameterType_Float, "margin", "Margin applied on the global vector database's envelope");
    SetParameterDescription("margin", "Margin applied on the global vector database's envelope.");
    SetDefaultParameterFloat("margin", 10.);
    MandatoryOff("margin");

    // Doc example parameter settings
    SetDocExampleParameterValue("im", "input_image.tif");
    SetDocExampleParameterValue("db", "input_vectorDB.shp");
    SetDocExampleParameterValue("out", "cropped_image.tif");
    SetDocExampleParameterValue("margin", "10.");
  }

  void DoUpdateParameters() override {}

  void DoExecute() override
  {
    GDALAllRegister();

    ROIExtractorFilterType::Pointer roiExtractorFilter = ROIExtractorFilterType::New();

    // Prepare inputs
    GDALDataset* imageSrcDS = (GDALDataset *) GDALOpen(GetParameterString("im").c_str(), GA_ReadOnly);
    GDALDataset* vectorDBSrcDS = (GDALDataset *) GDALOpenEx(GetParameterString("db").c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL);
    m_Margin = GetParameterFloat("margin");

    double cmin, cmax, lmin, lmax;
    OGRHelper::ExtractVectorDataEnvelope(imageSrcDS, vectorDBSrcDS, cmin, lmin, cmax, lmax);

    // Convert the input margin into number of pixels
    double* geoTransform = new double[6];
    imageSrcDS->GetGeoTransform(geoTransform);
    unsigned int paddingX = 0, paddingY = 0;
    if(geoTransform[1] != 0 && geoTransform[5] != 0) {
        paddingX = ceil(abs(m_Margin/geoTransform[1]));
        paddingY = ceil(abs(m_Margin/geoTransform[5]));
    }
    delete[] geoTransform;

    roiExtractorFilter->SetInput(GetParameterDoubleImage("im"));
    roiExtractorFilter->SetStartX(cmin);
    roiExtractorFilter->SetStartY(lmin);
    roiExtractorFilter->SetSizeX(cmax-cmin);
    roiExtractorFilter->SetSizeY(lmax-lmin);
    ImageSizeType padding;
    padding[0] = paddingX;
    padding[1] = paddingY;
    roiExtractorFilter->SetPadding(padding);
    roiExtractorFilter->Update();

    SetParameterOutputImage("out", roiExtractorFilter->GetOutput());

    GDALClose(imageSrcDS);
    GDALClose(vectorDBSrcDS);
  }

  double m_Margin;
};

} // namespace Wrapper
} // namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::CropAroundDB)
