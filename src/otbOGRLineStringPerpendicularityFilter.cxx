/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbMath.h"
#include "otbOGRLineStringPerpendicularityFilter.h"

namespace otb
{

OGRLineStringPerpendicularityFilter::OGRLineStringPerpendicularityFilter(const double& maxDistancePerpendicularity,
        const double& angleToleranceDeg)
: m_MaxDistancePerpendicularity(maxDistancePerpendicularity)
, m_AngleToleranceDeg(angleToleranceDeg) {
    m_AngleToleranceRad = m_AngleToleranceDeg * CONST_PI_180;
    m_AngleLow = CONST_PI_2 - m_AngleToleranceRad;
    m_AngleHigh = CONST_PI_2 + m_AngleToleranceRad;
}

// Filter the data by perpendicularity
void OGRLineStringPerpendicularityFilter::Filter() {
    for(std::list<OGRLineString>::iterator it = m_InputSegments.begin(); it != m_InputSegments.end(); it++) {
        OGRLineString currentLine = *it;
        for(std::list<OGRLineString>::iterator jt = std::next(it); jt != m_InputSegments.end(); jt++) {
            OGRLineString comparedLine = *jt;
            bool isCurrentAdded = false;

            // Compute distance
            if(currentLine.Distance(&comparedLine) < m_MaxDistancePerpendicularity) {
                // Compute angle
                double alpha = ComputeAngleBetweenLineSegments(currentLine, comparedLine);
                if(alpha >= m_AngleLow && alpha <= m_AngleHigh) {
                    if(!isCurrentAdded) {
                        m_OutputSegments.push_back(*it);
                        isCurrentAdded = true;
                    }
                    m_OutputSegments.push_back(*jt);
                    OGRPoint intersectPoint = ComputeLineSegmentsIntersectionPoint(*it, *jt);

                    // Add triplet to list
                    OGRTriplet currentTriplet;
                    currentTriplet.m_IntersectionPoint = intersectPoint;
                    currentTriplet.m_Segment1 = currentLine;
                    currentTriplet.m_Segment2 = comparedLine;
                    m_OutputTriplets.push_back(currentTriplet);
                }
            }
        }
    }
    m_OutputSegments.sort(OGRTriplet::LineStringComparison);
    m_OutputSegments.unique();
}

// Accessors
void OGRLineStringPerpendicularityFilter::SetAngleToleranceDeg(const double& angleToleranceDeg) {
    m_AngleToleranceDeg = angleToleranceDeg;
    m_AngleToleranceRad = m_AngleToleranceDeg * CONST_PI_180;
    m_AngleLow = CONST_PI_2 - m_AngleToleranceRad;
    m_AngleHigh = CONST_PI_2 + m_AngleToleranceRad;
}

// Compute the angle (in radian) between two line segments, assuming each line segments is composed of two points only
double OGRLineStringPerpendicularityFilter::ComputeAngleBetweenLineSegments(
		const OGRLineString& lineSeg1, const OGRLineString& lineSeg2) {
    double x1, x2, y1, y2, alpha;

    if(lineSeg1.getNumPoints() != 2 || lineSeg2.getNumPoints() != 2) { return 0.; }

    x1 = lineSeg1.getX(1) - lineSeg1.getX(0);
    y1 = lineSeg1.getY(1) - lineSeg1.getY(0);
    x2 = lineSeg2.getX(1) - lineSeg2.getX(0);
    y2 = lineSeg2.getY(1) - lineSeg2.getY(0);

    double num = x1 * x2 + y1 * y2;
    double den = sqrt(x1*x1 + y1*y1) * sqrt(x2*x2 + y2*y2);
    alpha = acos(num/den);

    if(alpha < 0) {
        alpha += CONST_PI;
    }

    return alpha;
}

// Compute the intersection point of two line segments, assuming each line segments is composed of two points only
OGRPoint OGRLineStringPerpendicularityFilter::ComputeLineSegmentsIntersectionPoint(
		const OGRLineString& lineSeg1, const OGRLineString& lineSeg2) {
    OGRPoint intersectionPoint;

    double a1 = lineSeg1.getY(1) - lineSeg1.getY(0);
    double b1 = lineSeg1.getX(0) - lineSeg1.getX(1);
    double c1 = a1*lineSeg1.getX(0) + b1*lineSeg1.getY(0);
    double a2 = lineSeg2.getY(1) - lineSeg2.getY(0);
    double b2 = lineSeg2.getX(0) - lineSeg2.getX(1);
    double c2 = a2*lineSeg2.getX(0) + b2*lineSeg2.getY(0);
    double det = a1*b2 - a2*b1;
    double det_inv = 1./det;

    if(det == 0) {
        intersectionPoint.setX(std::numeric_limits<double>::max());
        intersectionPoint.setY(std::numeric_limits<double>::max());
    } else {
        intersectionPoint.setX((b2*c1 - b1*c2) * det_inv);
        intersectionPoint.setY((a1*c2 - a2*c1) * det_inv);
    }

    return intersectionPoint;
}

} // namespace otb
