/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbOGRLineStringProximityFilter.h"

namespace otb
{

OGRLineStringProximityFilter::OGRLineStringProximityFilter(const double& maxDistanceToGeometry)
: m_MaxDistanceToGeometry(maxDistanceToGeometry)
, m_Geometry(NULL) {
}

// Filter the data by proximity to another geometry
void OGRLineStringProximityFilter::Filter() {
    // Get the extended envelope of the current geomtry tu be compared with those of each segment
    OGREnvelope* geometryEnvelope = new OGREnvelope;
    GetPaddedEnvelope(m_Geometry, geometryEnvelope, m_MaxDistanceToGeometry);
    OGREnvelope* segmentEnvelope = new OGREnvelope;

    for(auto&& lineString : m_InputSegments) {
        lineString.getEnvelope(segmentEnvelope);
        // As done in OGRLayer::SetSpatialFilter method: compare the envelopes of each segment and of the current geometry
        // If the segment is closed enough, we keep it, else it is ignored
        if( !(segmentEnvelope->MaxX < geometryEnvelope->MinX
                || segmentEnvelope->MaxY < geometryEnvelope->MinY
                || geometryEnvelope->MaxX < segmentEnvelope->MinX
                || geometryEnvelope->MaxY < segmentEnvelope->MinY) ) {
            m_OutputSegments.push_back(lineString);
        }
    }
}

// Get an extended envelope of a geometry
void OGRLineStringProximityFilter::GetPaddedEnvelope(OGRGeometry* geometry, OGREnvelope* envelope, const double& margin) {
    geometry->getEnvelope(envelope);

    envelope->MinX -= abs(margin);
    envelope->MaxX += abs(margin);
    envelope->MinY -= abs(margin);
    envelope->MaxY += abs(margin);
}


} // namespace otb
