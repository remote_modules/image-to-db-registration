/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbOGRHelper.h"
#include "otbOGRAffineTransformation.h"
#include "otbOGRDataSourceWrapper.h"
#include "otbOGRFeatureWrapper.h"
#include "itksys/SystemTools.hxx"

namespace otb
{

// Convert geo coordinates to pixel coordinates
void OGRHelper::ConvertGeoToPixel(double* geoTransform, const double& xp, const double& yp, double& c, double& l)  {
    c = (xp - geoTransform[0])/geoTransform[1];
    l = (yp - geoTransform[3])/geoTransform[5];
}

// Extract the min and max value (pixel) from the vector data
// This is done considering the geotransform of the input image
void OGRHelper::ExtractVectorDataEnvelope(GDALDataset* imageSrcDS, GDALDataset* vectorDataSrcDS,
        double& cmin, double& lmin, double& cmax, double& lmax) {
    double* geoTransform = new double[6];
    imageSrcDS->GetGeoTransform(geoTransform);

    // Upper left and lower right points (georeferenced coordinates)
    double ulx, uly, lrx, lry;
    // Upper left and lower right points (image coordinates)
    double ulc, ull, lrc, lrl;
    OGREnvelope* extent = new OGREnvelope();;
    OGRErr err = vectorDataSrcDS->GetLayer(0)->GetExtent(extent);
    ulx = extent->MinX;
    uly = extent->MinY;
    lrx = extent->MaxX;
    lry = extent->MaxY;

    // Convert ground to pixel
    ConvertGeoToPixel(geoTransform, ulx, uly, ulc, ull);
    ConvertGeoToPixel(geoTransform, lrx, lry, lrc, lrl);

    cmin = floor(std::min(ulc, lrc));
    cmax = floor(std::max(ulc, lrc));
    lmin = ceil(std::min(ull, lrl));
    lmax = ceil(std::max(ull, lrl));

    delete[] geoTransform;
    delete extent;
}

// Get the envelope of a geometry returned as a Polygon.
// This envelope can optionally be marged.
void OGRHelper::GetEnvelope(const OGRGeometry* geometry, OGRPolygon* polygon, double margin) {
    OGREnvelope *psEnvelope = new OGREnvelope();
    geometry->getEnvelope(psEnvelope);

    OGRLinearRing* ring = new OGRLinearRing();

    double envMinX = psEnvelope->MinX - abs(margin);
    double envMaxX = psEnvelope->MaxX + abs(margin);
    double envMinY = psEnvelope->MinY - abs(margin);
    double envMaxY = psEnvelope->MaxY + abs(margin);

    ring->addPoint(envMinX, envMinY);
    ring->addPoint(envMaxX, envMinY);
    ring->addPoint(envMaxX, envMaxY);
    ring->addPoint(envMinX, envMaxY);
    ring->addPoint(envMinX, envMinY);

    polygon->addRing(ring);
}

// Extract the list of the geometries that compose the vector DB
void OGRHelper::ExtractVectorDataGeometries(GDALDataset* vectorDataSrcDS, std::vector<OGRGeometry*>& geometries) {
    OGRLayer* layerSrc = vectorDataSrcDS->GetLayer(0);
    layerSrc->ResetReading();

    OGRFeature* feature;
    while((feature = layerSrc->GetNextFeature()) != NULL) {
        OGRGeometry* geometry = feature->GetGeometryRef();
        OGRGeometry* geometryC = geometry->clone();

        geometries.push_back(geometryC);
    }
}

std::list<OGRLineString> OGRHelper::ExtractSegmentsFromPolygon(OGRPolygon* ogrPolygon) {
    std::list<OGRLineString> segments;

    OGRLinearRing* ogrExternalRing = ogrPolygon->getExteriorRing();
    int nbPoints = ogrExternalRing->getNumPoints();
    for(unsigned int i = 0; i < nbPoints; i++) {
        OGRPoint p1, p2;
        OGRLineString segment;
        if(i < nbPoints - 1) {
            ogrExternalRing->getPoint(i, &p1);
            ogrExternalRing->getPoint(i+1, &p2);
        // Last segment that closes the polygon
        } else {
            ogrExternalRing->getPoint(ogrExternalRing->getNumPoints() - 1, &p1);
            ogrExternalRing->getPoint(0, &p2);
        }
        segment.addPoint(&p1);
        segment.addPoint(&p2);
        segments.push_back(segment);
    }

    OGRLinearRing* ogrInternalRing;
    for(int intRingIndex = 0; intRingIndex < ogrPolygon->getNumInteriorRings(); ++intRingIndex) {
        ogrInternalRing = ogrPolygon->getInteriorRing(intRingIndex);
        for(unsigned int i = 1; i < ogrInternalRing->getNumPoints() - 1; i++) {
            OGRPoint p1, p2;
            OGRLineString segment;
            if(i < nbPoints - 1) {
                ogrExternalRing->getPoint(i, &p1);
                ogrExternalRing->getPoint(i+1, &p2);
            // Last segment that closes the polygon
            } else {
                ogrExternalRing->getPoint(ogrExternalRing->getNumPoints() - 1, &p1);
                ogrExternalRing->getPoint(0, &p2);
            }
            segment.addPoint(&p1);
            segment.addPoint(&p2);
            segments.push_back(segment);
        }
    }

    return segments;
}

std::list<OGRLineString> OGRHelper::ExtractSegmentsFromMultiPolygon(OGRMultiPolygon* ogrMultiPolygon) {
    std::list<OGRLineString> segments;

    int nbGeometries = ogrMultiPolygon->getNumGeometries();
    for(int i = 0; i < nbGeometries; i++) {
        OGRPolygon* currentPolygon = (OGRPolygon*) ogrMultiPolygon->getGeometryRef(i);
        std::list<OGRLineString> currentSegments = ExtractSegmentsFromPolygon(currentPolygon);
        std::copy(currentSegments.begin(), currentSegments.end(),
                std::back_insert_iterator<std::list<OGRLineString> >(segments));
    }

    return segments;
}

std::list<OGRLineString> OGRHelper::ExtractSegmentsFromLineString(OGRLineString* ogrLineString) {
    std::list<OGRLineString> segments;

    int nbPoints = ogrLineString->getNumPoints();
    for(unsigned int i = 0; i < nbPoints; i++) {
        OGRPoint p1, p2;
        OGRLineString segment;
        if(i < nbPoints - 1) {
            ogrLineString->getPoint(i, &p1);
            ogrLineString->getPoint(i+1, &p2);
        // Last segment that closes the polygon
        } else {
            ogrLineString->getPoint(ogrLineString->getNumPoints() - 1, &p1);
            ogrLineString->getPoint(0, &p2);
        }
        segment.addPoint(&p1);
        segment.addPoint(&p2);
        segments.push_back(segment);
    }

    return segments;
}

std::list<OGRLineString> OGRHelper::ExtractSegmentsFromMultiLineString(OGRMultiLineString* ogrMultiLineString) {
    std::list<OGRLineString> segments;

    int nbGeometries = ogrMultiLineString->getNumGeometries();
    for(int i = 0; i < nbGeometries; i++) {
        OGRLineString* currentLineString = (OGRLineString*) ogrMultiLineString->getGeometryRef(i);
        std::list<OGRLineString> currentSegments = ExtractSegmentsFromLineString(currentLineString);
        std::copy(currentSegments.begin(), currentSegments.end(),
                std::back_insert_iterator<std::list<OGRLineString> >(segments));
    }

    return segments;
}

// Write result in shp file
void OGRHelper::WriteOGRVectorFile(const std::string& folder, const std::string& name, OGRSpatialReference* spatialRef,
        const std::list<OGRLineString>& segments) {
    otb::ogr::DataSource::Pointer ogrDS;
    otb::ogr::Layer layer(NULL, false);
    std::vector<std::string> options;

    ogrDS = otb::ogr::DataSource::New(folder + "/" + name, otb::ogr::DataSource::Modes::Overwrite);
    std::string layername = itksys::SystemTools::GetFilenameName(name);
    std::string extension = itksys::SystemTools::GetFilenameLastExtension(name);
    layername = layername.substr(0,layername.size()-extension.size());
    layer = ogrDS->CreateLayer(layername, spatialRef, wkbLineString, options);

    for(std::list<OGRLineString>::const_iterator it = segments.begin(); it != segments.end(); it++) {
        otb::ogr::Feature newFeature(layer.GetLayerDefn());
        newFeature.SetGeometry(&(*it));
        layer.CreateFeature(newFeature);
    }

    ogrDS->SyncToDisk();
}

void OGRHelper::WriteOGRVectorFile(const std::string& folder, const std::string& name, OGRSpatialReference* spatialRef,
        const std::list<OGRPoint>& segments) {
    otb::ogr::DataSource::Pointer ogrDS;
    otb::ogr::Layer layer(NULL, false);
    std::vector<std::string> options;

    ogrDS = otb::ogr::DataSource::New(folder + "/" + name, otb::ogr::DataSource::Modes::Overwrite);
    std::string layername = itksys::SystemTools::GetFilenameName(name);
    std::string extension = itksys::SystemTools::GetFilenameLastExtension(name);
    layername = layername.substr(0,layername.size()-extension.size());
    layer = ogrDS->CreateLayer(layername, spatialRef, wkbPoint, options);

    for(std::list<OGRPoint>::const_iterator it = segments.begin(); it != segments.end(); it++) {
        otb::ogr::Feature newFeature(layer.GetLayerDefn());
        newFeature.SetGeometry(&(*it));
        layer.CreateFeature(newFeature);
    }

    ogrDS->SyncToDisk();
}

// Apply an affine transformation on a OGRDataSource
void OGRHelper::ApplyTranformation(GDALDataset* vectorDataSrcDS, const std::string& folder, const std::string& name,
        OGRSpatialReference* spatialRef, const std::vector<double>& transform) {
    // Source layer
    OGRLayer* layerSrc = vectorDataSrcDS->GetLayer(0);
    layerSrc->ResetReading();

    // Destination layer
    otb::ogr::DataSource::Pointer ogrDS;
    otb::ogr::Layer layerDst(NULL, false);
    std::vector<std::string> options;
    ogrDS = otb::ogr::DataSource::New(folder + "/" + name, otb::ogr::DataSource::Modes::Overwrite);
    std::string layername = itksys::SystemTools::GetFilenameName(name);
    std::string extension = itksys::SystemTools::GetFilenameLastExtension(name);
    layername = layername.substr(0,layername.size()-extension.size());
    layerDst = ogrDS->CreateLayer(layername, spatialRef, wkbPolygon, options);

    OGRAffineTransformation affineTransform;
    affineTransform.SetTransformParameters(transform);

    // Transform each layer's feature
    OGRFeature* feature;
    while((feature = layerSrc->GetNextFeature()) != NULL) {
        OGRGeometry* geometry = feature->GetGeometryRef();
        OGRGeometry* geometryT = geometry->clone();
        geometryT->transform(&affineTransform);

        otb::ogr::Feature newFeature(layerDst.GetLayerDefn());
        newFeature.SetGeometry(geometryT);
        layerDst.CreateFeature(newFeature);
    }

    ogrDS->SyncToDisk();
}

} // namespace otb
