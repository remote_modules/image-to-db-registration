set(DOCUMENTATION "Image to vector database registration application")

otb_module(ImageToDBRegistration
  DEPENDS
    OTBCommon
    OTBApplicationEngine
    OTBIOGDAL
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine

  DESCRIPTION
    "${DOCUMENTATION}"
)
