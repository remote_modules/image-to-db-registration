/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OGR_LINESTRING_LENGTH_FILTER_H
#define OGR_LINESTRING_LENGTH_FILTER_H

#include "otbOGRLineStringFilter.h"

namespace otb
{

class OGRLineStringLengthFilter : public OGRLineStringFilter
{
public:
	OGRLineStringLengthFilter(const double& minLengthThreshold, const double& maxLengthThreshold);

    // Filter the data by length
    virtual void Filter();

    // Accessors
    void SetMinLengthThreshold(const double& minLenthThreshold) { m_MinLengthThreshold = minLenthThreshold; }
    double GetMinLengthThreshold() { return m_MinLengthThreshold; }
    void SetMaxLengthThreshold(const double& maxLenthThreshold) { m_MaxLengthThreshold = maxLenthThreshold; }
    double GetMaxLengthThreshold() { return m_MaxLengthThreshold; }

protected:
    // Min and max threshold
    double m_MinLengthThreshold;
    double m_MaxLengthThreshold;
};


} // namespace otb

#endif
