/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "otbMacro.h"
#include "otbROIExtractorImageFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageRegionIterator.h"

namespace otb
{

/** Constructor */
template <class TInputImage, class TOutputImage>
ROIExtractorImageFilter<TInputImage, TOutputImage>::ROIExtractorImageFilter()
: m_StartX(0)
, m_StartY(0)
, m_SizeX(0)
, m_SizeY(0) {
    m_Padding[0] = 0;
    m_Padding[1] = 0;
}


template <class TInputImage, class TOutputImage>
void ROIExtractorImageFilter<TInputImage, TOutputImage>::GenerateInputRequestedRegion() {
    // Call superclass implementation
    Superclass::GenerateInputRequestedRegion();
    InputImagePointer inputImage = const_cast<InputImageType*>(this->GetInput());

    // The input is necessarily the largest possible region.
    inputImage->SetRequestedRegionToLargestPossibleRegion();
}

template <class TInputImage, class TOutputImage>
void ROIExtractorImageFilter<TInputImage, TOutputImage>::AllocateOutputs() {
	otbLogMacro(Info, << "AllocateOutputs()");
}

template <class TInputImage, class TOutputImage>
void ROIExtractorImageFilter<TInputImage, TOutputImage>::GenerateData() {
    InputImageConstPointer inputImage = this->GetInput();
    OutputImagePointer outputImage = this->GetOutput();
    InputImageRegionType requestedRegion;
    InputImageIndexType start;
    InputImageSizeType size;
    start[0] = m_StartX;
    start[1] = m_StartY;
    size[0] = m_SizeX;
    size[1] = m_SizeY;
    requestedRegion.SetSize(size);
    requestedRegion.SetIndex(start);
    // Add padding
    requestedRegion.PadByRadius(m_Padding);

    InputImageIndexType largestPossibleIndex = inputImage->GetLargestPossibleRegion().GetIndex();
    InputImageSizeType  largestPossibleSize  = inputImage->GetLargestPossibleRegion().GetSize();
    InputImageIndexType requestedIndex = requestedRegion.GetIndex();
    InputImageSizeType  requestedSize  = requestedRegion.GetSize();

    OutputImageRegionType outputRegion;
    OutputImageIndexType outputIndexMinPoint;
    OutputImageIndexType outputIndexMaxPoint;
    OutputImageSizeType outputSize;

    // Check that the output region is not outside the input largest possible region
    outputIndexMinPoint[0] = std::max(largestPossibleIndex[0], requestedIndex[0]);
    outputIndexMinPoint[1] = std::max(largestPossibleIndex[1], requestedIndex[1]);
    outputIndexMaxPoint[0] = std::min(largestPossibleIndex[0] + largestPossibleSize[0], requestedIndex[0] + requestedSize[0]);
    outputIndexMaxPoint[1] = std::min(largestPossibleIndex[1] + largestPossibleSize[1], requestedIndex[1] + requestedSize[1]);
    outputSize[0] = outputIndexMaxPoint[0] - outputIndexMinPoint[0];
    outputSize[1] = outputIndexMaxPoint[1] - outputIndexMinPoint[1];

    // Allocate output image
    outputRegion.SetSize(outputSize);
    outputRegion.SetIndex(outputIndexMinPoint);
    outputImage->SetRegions(outputRegion);
    outputImage->SetOrigin(inputImage->GetOrigin());
    outputImage->Allocate();
    outputImage->SetProjectionRef(inputImage->GetProjectionRef());
    outputImage->SetSignedSpacing(inputImage->GetSignedSpacing());

    // Update the requested region to take into account the limits previously computed
    requestedRegion = outputRegion;

    typedef itk::ImageRegionConstIterator<InputImageType> ConstIteratorType;
    typedef itk::ImageRegionIterator<OutputImageType>     IteratorType;

    ConstIteratorType inputIt(inputImage, requestedRegion);
    IteratorType outputIt(outputImage, outputRegion);
    for(inputIt.GoToBegin(), outputIt.GoToBegin(); !inputIt.IsAtEnd(); ++inputIt, ++outputIt) {
        outputIt.Set(inputIt.Get());
    }
}

template <class TInputImage, class TOutputImage>
void ROIExtractorImageFilter<TInputImage, TOutputImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
{
    Superclass::PrintSelf(os, indent);
    os << indent << "StartX: " << m_StartX << std::endl;
    os << indent << "StartY: " << m_StartY << std::endl;
    os << indent << "SizeX: " << m_SizeX << std::endl;
    os << indent << "SizeY: " << m_SizeY << std::endl;
    os << indent << "PaddingX: " << m_Padding[0] << std::endl;
    os << indent << "PaddingY: " << m_Padding[1] << std::endl;
}

} // namespace otb
