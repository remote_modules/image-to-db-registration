/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef ROI_EXTRACTOR_IMAGE_FILTER_H
#define ROI_EXTRACTOR_IMAGE_FILTER_H

#include "itkImageToImageFilter.h"
#include "otbImage.h"


namespace otb
{

template <class TInputImage, class TOutputImage = TInputImage>
class ROIExtractorImageFilter: public itk::ImageToImageFilter<TInputImage, TOutputImage>
{
public:
    /** Additional typedefs for the input image. */
    typedef TInputImage                           InputImageType;
    typedef typename InputImageType::Pointer      InputImagePointer;
    typedef typename InputImageType::ConstPointer InputImageConstPointer;
    typedef typename InputImageType::RegionType   InputImageRegionType;
    typedef typename InputImageType::PixelType    InputImagePixelType;
    typedef typename InputImageType::SizeType     InputImageSizeType;
    typedef typename InputImageType::IndexType    InputImageIndexType;
    /** Additional typedefs for the output image. */
    typedef TOutputImage                          OutputImageType;
    typedef typename OutputImageType::Pointer     OutputImagePointer;
    typedef typename OutputImageType::RegionType  OutputImageRegionType;
    typedef typename OutputImageType::PixelType   OutputImagePixelType;
    typedef typename OutputImageType::SizeType    OutputImageSizeType;
    typedef typename OutputImageType::IndexType   OutputImageIndexType;

    //  Standard declarations, used for object creation with the object factory:
    using Self         = ROIExtractorImageFilter<InputImageType>;
    using Superclass   = itk::ImageToImageFilter<InputImageType, OutputImageType>;
    using Pointer      = itk::SmartPointer<Self>;
    using ConstPointer = itk::SmartPointer<const Self>;

    /** Method for creation through object factory */
    itkNewMacro(Self);

    /** Run-time type information */
    itkTypeMacro(ROIExtractorImageFilter, ImageToImageFilter);

    /** Display */
    void PrintSelf(std::ostream& os, itk::Indent indent) const override;

    /** Accessors */
    itkSetMacro(StartX,  int);
    itkGetMacro(StartX,  int);
    itkSetMacro(StartY,  int);
    itkGetMacro(StartY,  int);
    itkSetMacro(SizeX,   int);
    itkGetMacro(SizeX,   int);
    itkSetMacro(SizeY,   int);
    itkGetMacro(SizeY,   int);
    itkSetMacro(Padding, InputImageSizeType);
    itkGetMacro(Padding, InputImageSizeType);

protected:
    ROIExtractorImageFilter();
    virtual ~ROIExtractorImageFilter() override {};

    void GenerateInputRequestedRegion() override;
    void AllocateOutputs() override;
    void GenerateData() override;

private:
    ROIExtractorImageFilter(Self&) = delete;
    void operator=(const Self&) = delete;

    int m_StartX;
    int m_StartY;
    int m_SizeX;
    int m_SizeY;
    InputImageSizeType m_Padding;
};

} // namespace otb

#include "otbROIExtractorImageFilter.txx"

#endif // ROI_EXTRACTOR_IMAGE_FILTER_H
