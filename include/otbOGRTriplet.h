/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OGR_TRIPLET_H
#define OGR_TRIPLET_H

#include "ogr_geometry.h"
#include "gdal_priv.h"
#include "ogrsf_frmts.h"

namespace otb
{

class OGRTriplet
{
public:
    OGRLineString m_Segment1;
    OGRLineString m_Segment2;
    OGRPoint m_IntersectionPoint;

    bool operator==(const OGRTriplet& other) {
        return (   m_IntersectionPoint == other.m_IntersectionPoint
                && m_Segment1 == other.m_Segment1
                && m_Segment2 == other.m_Segment2);
    }

    static bool LineStringComparison(OGRLineString first, OGRLineString second) {
        if(first.getX(0) == second.getX(0)) {
            if(first.getY(0) == second.getY(0)) {
                if(first.getX(1) == second.getX(1)) {
                    return first.getY(1) == second.getY(1);
                } else {
                    return (first.getX(1) > second.getX(1));
                }
            } else {
                return (first.getY(0) > second.getY(0));
            }
        } else {
            return (first.getX(0) > second.getX(0));
        }
    }
    // Used to sort Triplets
    static bool TripletComparison(OGRTriplet first, OGRTriplet second) {
        if(first.m_IntersectionPoint.getX() == second.m_IntersectionPoint.getX()) {
            if(first.m_IntersectionPoint.getY() == second.m_IntersectionPoint.getY()) {
                if(first.m_Segment1 == second.m_Segment1) {
                    return LineStringComparison(first.m_Segment2, second.m_Segment2);
                } else {
                    return LineStringComparison(first.m_Segment1, second.m_Segment1);
                }
            } else {
                return first.m_IntersectionPoint.getY() > second.m_IntersectionPoint.getY();
            }
        } else {
            return first.m_IntersectionPoint.getX() > second.m_IntersectionPoint.getX();
        }
    }
};

} // namespace otb

#endif
