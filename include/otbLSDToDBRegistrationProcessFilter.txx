/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbMacro.h"
#include "otbStopwatch.h"
#include "itkImageRegionIterator.h"
#include "otbOGRAffineTransformation.h"

#include <algorithm>
#include "otbLSDToDBRegistrationProcessFilter.h"
#include "itksys/SystemTools.hxx"

namespace otb
{

template <class TDataType>
LSDToDBRegistrationProcessFilter<TDataType>
::LSDToDBRegistrationProcessFilter()
: m_MinLengthThreshold(0.)
, m_MaxLengthThreshold(std::numeric_limits<double>::max())
, m_MaxDistanceToVectorDB(0.)
, m_MaxDistancePerpendicularity(0.)
, m_AngleToleranceDeg(0.)
, m_GenerateIntermediateData(false)
, m_LengthFilter(NULL)
, m_ProximityFilter(NULL)
, m_PerpendicularityFilter(NULL)
, m_spatialRef(NULL) {
    GDALAllRegister();
}

template <class TDataType>
void
LSDToDBRegistrationProcessFilter<TDataType>
::ExtractAndFilterSegmentsFromVectorDataBase(GDALDataset* vectorDataSrcDS,
		std::list<OGRLineString>& filteredSegments, std::list<OGRTriplet>& filteredTriplets) {
    OGRLayer* layer = vectorDataSrcDS->GetLayer(0);
    layer->ResetReading();

    OGRFeature* feature;
    while((feature = layer->GetNextFeature()) != NULL) {
        OGRGeometry* currentGeometry = feature->GetGeometryRef();
        std::list<OGRLineString> currentSegments;
        std::list<OGRTriplet> currentTriplets;
        switch(currentGeometry->getGeometryType()) {
            case wkbPolygon:
                currentSegments = OGRHelper::ExtractSegmentsFromPolygon((OGRPolygon*)currentGeometry);
                break;
            case wkbMultiPolygon:
                currentSegments = OGRHelper::ExtractSegmentsFromMultiPolygon((OGRMultiPolygon*)currentGeometry);
                break;
            case wkbLineString:
                currentSegments = OGRHelper::ExtractSegmentsFromLineString((OGRLineString*)currentGeometry);
                break;
            case wkbMultiLineString:
                currentSegments = OGRHelper::ExtractSegmentsFromMultiLineString((OGRMultiLineString*)currentGeometry);
                break;
        }
        // Filter by length
        m_LengthFilter->SetInputSegments(currentSegments);
        m_LengthFilter->Filter();
        currentSegments = m_LengthFilter->GetOutputSegments();
        m_LengthFilter->ClearOutputs();

        // Filter by perpendicularity
        m_PerpendicularityFilter->SetInputSegments(currentSegments);
        m_PerpendicularityFilter->Filter();
        currentSegments = m_PerpendicularityFilter->GetOutputSegments();
        currentTriplets = m_PerpendicularityFilter->GetOutputTriplets();
        m_PerpendicularityFilter->ClearOutputs();

        std::copy(
                currentSegments.begin(),
                currentSegments.end(),
                std::back_insert_iterator<std::list<OGRLineString> >(filteredSegments));
        std::copy(
                currentTriplets.begin(),
                currentTriplets.end(),
                std::back_insert_iterator<std::list<OGRTriplet> >(filteredTriplets));
    }
}

template <class TDataType>
void
LSDToDBRegistrationProcessFilter<TDataType>
::ExtractAndFilterSegmentsFromLSD(GDALDataset* lsdfSrcDS, std::vector<OGRGeometry*> geometries,
		std::list<OGRLineString>& lsdSegments, std::list<OGRTriplet>& lsdTriplets) {
    OGRLayer* layer = lsdfSrcDS->GetLayer(0);
    layer->ResetReading();

    // 1. Extract segments from lsd datasource file and filter by length
    OGRFeature* feature;
    while((feature = layer->GetNextFeature()) != NULL) {
        OGRGeometry* currentGeometry = feature->GetGeometryRef();
        std::list<OGRLineString> currentSegments;
        std::list<OGRTriplet> currentTriplets;
        switch(currentGeometry->getGeometryType()) {
            case wkbLineString:
                currentSegments = OGRHelper::ExtractSegmentsFromLineString((OGRLineString*)currentGeometry);
                break;
            case wkbMultiLineString:
                currentSegments = OGRHelper::ExtractSegmentsFromMultiLineString((OGRMultiLineString*)currentGeometry);
                break;
        }
        // Filter by length
        m_LengthFilter->SetInputSegments(currentSegments);
        m_LengthFilter->Filter();
    }
    lsdSegments = m_LengthFilter->GetOutputSegments();
	m_LengthFilter->ClearOutputs();

    if (m_GenerateIntermediateData) {
        OGRHelper::WriteOGRVectorFile(m_WorkingDir, "lsd_lengthFilter" + m_Extension, m_spatialRef, lsdSegments);
    }
    otbLogMacro(Info, << "Length filtering : " << lsdSegments.size() << " segments left");

    // 2. Check distances between the filtered segments and each geometry from input database,
    // and process perpendicularity filter
    std::list<OGRLineString> filteredSegments;
    for(auto && geometry : geometries) {
        std::list<OGRLineString> currentKeptImageSegments = lsdSegments;
        std::list<OGRTriplet> currentKeptImageTriplets;

        // Filter by proximity
        m_ProximityFilter->SetGeometry(geometry);
        m_ProximityFilter->SetInputSegments(currentKeptImageSegments);
        m_ProximityFilter->Filter();
        currentKeptImageSegments = m_ProximityFilter->GetOutputSegments();
        m_ProximityFilter->ClearOutputs();

        // Filter by perpendicularity
        m_PerpendicularityFilter->SetInputSegments(currentKeptImageSegments);
        m_PerpendicularityFilter->Filter();
        currentKeptImageSegments = m_PerpendicularityFilter->GetOutputSegments();
        currentKeptImageTriplets = m_PerpendicularityFilter->GetOutputTriplets();
        m_PerpendicularityFilter->ClearOutputs();

        filteredSegments.insert(filteredSegments.end(), currentKeptImageSegments.begin(), currentKeptImageSegments.end());
        lsdTriplets.insert(lsdTriplets.end(), currentKeptImageTriplets.begin(), currentKeptImageTriplets.end());
    }
    filteredSegments.sort(OGRTriplet::LineStringComparison);
    filteredSegments.unique();
    lsdSegments = filteredSegments;
    lsdTriplets.sort(OGRTriplet::TripletComparison);
    lsdTriplets.unique();
}

template <class TDataType>
std::vector<double>
LSDToDBRegistrationProcessFilter<TDataType>
::ComputeBestTransformation(const std::list<OGRTriplet>& imgTriplets, const std::list<OGRLineString>& imgSegments,
        const std::list<OGRTriplet>& vectorDBTriplets, const std::list<OGRLineString>& vectorDBSegments) {
    std::vector<double> bestTransformation(6, 0.);

    // Initialize with identity transformation
    bestTransformation[0] = 1.;
    bestTransformation[1] = 0.;
    bestTransformation[2] = 0.;
    bestTransformation[3] = 0.;
    bestTransformation[4] = 1.;
    bestTransformation[5] = 0.;

    // Tool used to transform OGR objects
    OGRAffineTransformation affineTransform;

    double bestCumulatedDist = std::numeric_limits<double>::max();

    unsigned int currentProgress = 0;
    unsigned int tripletsProcessedNb = 0;
    unsigned int totalTriplets = imgTriplets.size();

    for(auto && imgT : imgTriplets) {

    	double progress = 100 * static_cast<double>(tripletsProcessedNb)/static_cast<double>(totalTriplets);
        while(progress >= currentProgress) {
          otbLogMacro(Info, << "Progress: " << currentProgress << "% (" << tripletsProcessedNb << " image triplets already processed)");
          currentProgress+=10;
        }

        for(auto && vectorDBT : vectorDBTriplets) {
            if(abs(imgT.m_IntersectionPoint.getX() - vectorDBT.m_IntersectionPoint.getX()) < m_MaxDistanceToVectorDB) {
                if(abs(imgT.m_IntersectionPoint.getY() - vectorDBT.m_IntersectionPoint.getY()) < m_MaxDistanceToVectorDB) {

                    std::vector<double> currentTransform(6, 0.);
                    // No rotation, only a translation is computed
                    currentTransform[0] = 1.;
                    currentTransform[1] = 0.;
                    currentTransform[2] = imgT.m_IntersectionPoint.getX() - vectorDBT.m_IntersectionPoint.getX();
                    currentTransform[3] = 0.;
                    currentTransform[4] = 1.;
                    currentTransform[5] = imgT.m_IntersectionPoint.getY() - vectorDBT.m_IntersectionPoint.getY();
                    affineTransform.SetTransformParameters(currentTransform);

                    // Translate each segment from the vector database and compare them to the image segments
                    double cumulatedDist = 0.;
                    for(auto && vectorDBS : vectorDBSegments) {
                        OGRLineString* ptrVectorDBS = (OGRLineString*)vectorDBS.clone();
                        ptrVectorDBS->transform(&affineTransform);
                        OGRPoint pointVSA, pointVSB;
                        ptrVectorDBS->getPoint(0, &pointVSA);
                        ptrVectorDBS->getPoint(1, &pointVSB);

                        double minInlierDist = m_MaxDistanceToVectorDB;

                        OGREnvelope* vectorDBSEnvelope = new OGREnvelope;
                        OGREnvelope* imgSEnvelope = new OGREnvelope;
                        ptrVectorDBS->getEnvelope(vectorDBSEnvelope);

                        for(auto && imgS : imgSegments) {
                            imgS.getEnvelope(imgSEnvelope);
                            // Check that both segments are closed to each other.
                            if( !(imgSEnvelope->MaxX < vectorDBSEnvelope->MinX
                                    || imgSEnvelope->MaxY < vectorDBSEnvelope->MinY
                                    || vectorDBSEnvelope->MaxX < imgSEnvelope->MinX
                                    || vectorDBSEnvelope->MaxY < imgSEnvelope->MinY) ) {

                                OGRPoint pointISA, pointISB;
                                imgS.getPoint(0, &pointISA);
                                imgS.getPoint(1, &pointISB);
                                // Compute the min point-to-point distance between
                                // the current vectorDB segment and the current image segment.
                                double inlierDist1 = std::min(pointVSA.Distance(&pointISA), pointVSA.Distance(&pointISB));
                                double inlierDist2 = std::min(pointVSB.Distance(&pointISA), pointVSB.Distance(&pointISB));
                                double inlierDist = inlierDist1 + inlierDist2;
                                if(inlierDist < minInlierDist) {
                                    minInlierDist = inlierDist;
                                }
                            }
                        }
                        cumulatedDist += minInlierDist;
                    }

                    if(cumulatedDist < bestCumulatedDist) {
                        bestCumulatedDist = cumulatedDist;
                        bestTransformation = currentTransform;
                    }
                }
            }
        }
        tripletsProcessedNb++;
    }

    otbLogMacro(Info, <<"Progress: 100%");

    return bestTransformation;
}

template <class TDataType>
void
LSDToDBRegistrationProcessFilter<TDataType>
::Process() {
    otbLogMacro(Info, << "LSDToDBRegistrationProcessFilter : Generate data..." << std::endl);

    m_Extension = itksys::SystemTools::GetFilenameLastExtension(m_InputLSDFileName);

    // Initialize OGR Filters
    m_LengthFilter           = new OGRLineStringLengthFilter(m_MinLengthThreshold, m_MaxLengthThreshold);
    m_ProximityFilter        = new OGRLineStringProximityFilter(m_MaxDistanceToVectorDB);
    m_PerpendicularityFilter = new OGRLineStringPerpendicularityFilter(m_MaxDistancePerpendicularity, m_AngleToleranceDeg);

    // Read input data
    GDALDataset* vectorDBSrcDS = (GDALDataset *) GDALOpenEx(m_InputVectorDBName.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL);
    GDALDataset* lsdSrcDS      = (GDALDataset *) GDALOpenEx(m_InputLSDFileName.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL);
    m_spatialRef= lsdSrcDS->GetLayer(0)->GetSpatialRef();

    // Lists of segments and triplets
    std::list<OGRLineString> lsdSegments;
    std::list<OGRTriplet>    lsdTriplets;
    std::list<OGRLineString> vectorDataSegments;
    std::list<OGRTriplet>    vectorDataTriplets;
/////////////////////////////////////////////////

/////////////////////////////////////////////////
    // 1. Extract perpendicular segments from input vector data
    otbLogMacro(Info, << "Step 1: Vector database segments filtering...");

    ExtractAndFilterSegmentsFromVectorDataBase(vectorDBSrcDS, vectorDataSegments, vectorDataTriplets);
    if (m_GenerateIntermediateData) {
        OGRHelper::WriteOGRVectorFile(m_WorkingDir, "db_perpendicularFiltering" + m_Extension, m_spatialRef, vectorDataSegments);
        std::list<OGRPoint> pointsList;
        for(std::list<OGRTriplet>::iterator it = vectorDataTriplets.begin(); it != vectorDataTriplets.end(); it++) {
            pointsList.push_back(it->m_IntersectionPoint);
        }
        OGRHelper::WriteOGRVectorFile(m_WorkingDir, "db_intersectionPoints" + m_Extension, m_spatialRef, pointsList);
    }
    otbLogMacro(Info, << "Database filtering : " << vectorDataSegments.size() << " segments left");
    otbLogMacro(Info, << "Database filtering : " << vectorDataTriplets.size() << " triplets remained" << std::endl);
/////////////////////////////////////////////////

/////////////////////////////////////////////////
    // 2. Extract perpendicular segments from input LSD
    otbLogMacro(Info, << "Step 2: LSD segments filtering...");
    std::vector<OGRGeometry*> geometries;
    OGRHelper::ExtractVectorDataGeometries(vectorDBSrcDS, geometries);

    ExtractAndFilterSegmentsFromLSD(lsdSrcDS, geometries,lsdSegments, lsdTriplets);
    if (m_GenerateIntermediateData) {
        OGRHelper::WriteOGRVectorFile(m_WorkingDir, "lsd_perpendicularityFilter" + m_Extension, m_spatialRef, lsdSegments);
        std::list<OGRPoint> pointsList;
        for(std::list<OGRTriplet>::iterator it = lsdTriplets.begin(); it != lsdTriplets.end(); it++) {
            pointsList.push_back(it->m_IntersectionPoint);
        }
        OGRHelper::WriteOGRVectorFile(m_WorkingDir, "lsd_intersectionPoints" + m_Extension, m_spatialRef, pointsList);
    }
    otbLogMacro(Info, << "LSD filtering : " << lsdSegments.size() << " segments left");
    otbLogMacro(Info, << "LSD filtering : " << lsdTriplets.size() << " triplets remained" << std::endl);
/////////////////////////////////////////////////

/////////////////////////////////////////////////
    // 3. Registration
    // 3.a Compute the best Transformation
    otbLogMacro(Info, << "Step 3a : Compute best transformation...");
    std::vector<double> transformation = ComputeBestTransformation(lsdTriplets, lsdSegments, vectorDataTriplets, vectorDataSegments);
    otbLogMacro(Info, << "Transformation : " << std::endl
            << "\tR = [" << transformation[0] << " " << transformation[1] << ", " << transformation[3] << " " << transformation[4] << "]" << std::endl
            << "\tT = [" << transformation[2] << ", " << transformation[5] << "]" << std::endl
    );

    // 3.b Apply the transformation to the vector DB
    OGRHelper::ApplyTranformation(vectorDBSrcDS, m_OutputDir, m_OutputVectorDBName, m_spatialRef, transformation);
/////////////////////////////////////////////////

/////////////////////////////////////////////////
    GDALClose(vectorDBSrcDS);
    GDALClose(lsdSrcDS);

    delete m_LengthFilter;
    m_LengthFilter = NULL;
    delete m_ProximityFilter;
    m_ProximityFilter = NULL;
    delete m_PerpendicularityFilter;
    m_PerpendicularityFilter = NULL;
}

template <class TDataType>
void
LSDToDBRegistrationProcessFilter<TDataType>
::PrintSelf(std::ostream& os, itk::Indent indent) const {
    Superclass::PrintSelf(os, indent);
}

}
