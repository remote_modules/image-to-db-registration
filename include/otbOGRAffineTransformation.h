/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OGR_AFFINE_TRANSFORMATION_H
#define OGR_AFFINE_TRANSFORMATION_H

#include "ogr_spatialref.h"

class OGRAffineTransformation : public OGRCoordinateTransformation {
public:
    OGRAffineTransformation()
    : OGRCoordinateTransformation()
    , m_r11(1.)
    , m_r12(0.)
    , m_t1(0.)
    , m_r21(0.)
    , m_r22(1.)
    , m_t2(0.) {}

    /** Fetch internal source coordinate system. */
    virtual OGRSpatialReference *GetSourceCS() { return NULL; }
    /** Fetch internal target coordinate system. */
    virtual OGRSpatialReference *GetTargetCS() { return NULL; }

    int Transform(int nCount, double* x, double* y, double* z = nullptr, int* pabSuccess = nullptr) {
        for(int i = 0; i < nCount; i++) {
            double xt = m_r11*x[i] + m_r12*y[i] + m_t1;
            double yt = m_r21*x[i] + m_r22*y[i] + m_t2;
            x[i] = xt;
            y[i] = yt;
            if(pabSuccess) {
                pabSuccess[i] = true;
            }
        }
        return true;
    }

    virtual int Transform( int nCount, double *x, double *y, double *z, double *t, int *pabSuccess)    {
        return this->Transform(nCount, x, y, nullptr, pabSuccess);
    }

    void SetTransformParameters(const std::vector<double>& transformParameters) {
        if(transformParameters.size() == 6) {
            m_r11 = transformParameters[0];
            m_r12 = transformParameters[1];
            m_t1  = transformParameters[2];
            m_r21 = transformParameters[3];
            m_r22 = transformParameters[4];
            m_t2  = transformParameters[5];
        }
    }

    virtual OGRCoordinateTransformation* Clone() const {
        return new OGRAffineTransformation(*this);
    }


private:
    // Affine Transformation parameters :
    // p' = R*p + T with R = [r11 r12, r21 r22] and T = [t1, t2]
    double m_r11;
    double m_r12;
    double m_r21;
    double m_r22;
    double m_t1;
    double m_t2;
};

#endif
