/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OGR_LINESTRING_PROXIMITY_FILTER_H
#define OGR_LINESTRING_PROXIMITY_FILTER_H

#include "otbOGRLineStringFilter.h"

namespace otb
{

class OGRLineStringProximityFilter : public OGRLineStringFilter
{
public:
	OGRLineStringProximityFilter(const double& maxDistanceToGeometry);

    // Filter the data  by proximity to another geometry
    virtual void Filter();

    // Accessors
    void SetMaxDistanceToGeometry(const double& maxDistanceToGeometry) { m_MaxDistanceToGeometry = maxDistanceToGeometry; }
    double GetMaxDistanceToGeometry() { return m_MaxDistanceToGeometry; }
    void SetGeometry(OGRGeometry* geometry) { m_Geometry = geometry; }
    OGRGeometry* GetGeometry() { return m_Geometry; }

protected:
    // Get an extended envelope of a geometry
    void GetPaddedEnvelope(OGRGeometry* geometry, OGREnvelope* envelope, const double& margin);

    double m_MaxDistanceToGeometry;
    OGRGeometry* m_Geometry;
};


} // namespace otb

#endif
