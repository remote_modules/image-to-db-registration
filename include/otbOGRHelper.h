/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OGR_HELPER_H
#define OGR_HELPER_H

#include "otbOGRTriplet.h"
#include "ogr_geometry.h"
#include "gdal_priv.h"
#include "ogrsf_frmts.h"
#include <list>

namespace otb
{

class OGRHelper {

public:
    // Convert geo coordinates to pixel coordinates
    static void ConvertGeoToPixel(double* geoTransform, const double& xp, const double& yp, double& c, double& l);

    // Extract the min and max value (pixel) from the vector data
    // This is done considering the geotransform of the input image
    static void ExtractVectorDataEnvelope(GDALDataset* imageSrcDS, GDALDataset* vectorDataSrcDS,
            double& cmin, double& lmin, double& cmax, double& lmax);

    // Get the envelope of a geometry returned as a Polygon.
	// This envelope can optionally be marged.
	static void GetEnvelope(const OGRGeometry* geometry, OGRPolygon* polygon, double margin = 0);

    // Extract the list of the geometries that compose the vector DB
    static void ExtractVectorDataGeometries(GDALDataset* vectorDataSrcDS, std::vector<OGRGeometry*>& geometries);
    // Extract the segments that compose geometries
    static std::list<OGRLineString> ExtractSegmentsFromPolygon(OGRPolygon* ogrPolygon);
    static std::list<OGRLineString> ExtractSegmentsFromMultiPolygon(OGRMultiPolygon* ogrMultiPolygon);
    static std::list<OGRLineString> ExtractSegmentsFromLineString(OGRLineString* ogrLineString);
    static std::list<OGRLineString> ExtractSegmentsFromMultiLineString(OGRMultiLineString* ogrMultiLineString);

    // Write result in shp file
    static void WriteOGRVectorFile(const std::string& folder, const std::string& name, OGRSpatialReference* spatialRef,
    		const std::list<OGRLineString>& segments);
    static void WriteOGRVectorFile(const std::string& folder, const std::string& name, OGRSpatialReference* spatialRef,
    		const std::list<OGRPoint>& segments);

    // Apply an affine transformation on a OGRDataSource
    static void ApplyTranformation(GDALDataset* vectorDataSrcDS, const std::string& folder, const std::string& name,
            OGRSpatialReference* spatialRef, const std::vector<double>& transform);
};

} // namespace otb

#endif
