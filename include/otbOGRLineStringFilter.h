/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OGR_LINESTRING_FILTER_H
#define OGR_LINESTRING_FILTER_H

#include "ogr_geometry.h"
#include <list>

namespace otb
{

class OGRLineStringFilter
{
public:
	virtual ~OGRLineStringFilter() {}

    // Filter the data
	virtual void Filter() = 0;

    // Accessors
    void SetInputSegments(const std::list<OGRLineString>& inputSegments) { m_InputSegments = inputSegments; }
    std::list<OGRLineString> GetOutputSegments() { return m_OutputSegments; }

    virtual void ClearOutputs() {m_OutputSegments.clear(); }

protected:
    // Input segments to be filtered
    std::list<OGRLineString> m_InputSegments;
    // Filtered segments
    std::list<OGRLineString> m_OutputSegments;
};


} // namespace otb

#endif
