/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef LSD_TO_DB_REGISTRATION_PROCESS_FILTER_H
#define LSD_TO_DB_REGISTRATION_PROCESS_FILTER_H

#include "itkObject.h"

// OGR Filters
#include "otbOGRHelper.h"
#include "otbOGRLineStringLengthFilter.h"
#include "otbOGRLineStringProximityFilter.h"
#include "otbOGRLineStringPerpendicularityFilter.h"

namespace otb
{

template <class TDataType = double>
class ITK_EXPORT LSDToDBRegistrationProcessFilter : public itk::Object
{
public:
    /** Standard class typedefs. */
    typedef LSDToDBRegistrationProcessFilter Self;
    typedef itk::Object                      Superclass;
    typedef itk::SmartPointer<Self>          Pointer;
    typedef itk::SmartPointer<const Self>    ConstPointer;

    typedef TDataType                        DataType;

    /** Run-time type information (and related methods). */
    itkTypeMacro(LSDToDBRegistrationProcessFilter, itk::Object);

    /** Method for creation through the object factory. */
    itkNewMacro(Self);

    // Parameters accessors
    itkSetMacro(InputLSDFileName, std::string);
    itkSetMacro(InputVectorDBName, std::string);
    itkSetMacro(OutputDir, std::string);
    itkSetMacro(OutputVectorDBName, std::string);

    itkSetMacro(MinLengthThreshold, DataType);
    itkGetMacro(MinLengthThreshold, DataType);
    itkSetMacro(MaxLengthThreshold, DataType);
    itkGetMacro(MaxLengthThreshold, DataType);
	itkSetMacro(MaxDistanceToVectorDB, DataType);
	itkGetMacro(MaxDistanceToVectorDB, DataType);
    itkSetMacro(MaxDistancePerpendicularity, DataType);
    itkGetMacro(MaxDistancePerpendicularity, DataType);
    itkSetMacro(AngleToleranceDeg, DataType);
    itkGetMacro(AngleToleranceDeg, DataType);

    void SetWorkingDir(const std::string& workingDir){
        m_GenerateIntermediateData = true;
        m_WorkingDir = workingDir;
    }

    void Process();
protected:
    LSDToDBRegistrationProcessFilter();
    ~LSDToDBRegistrationProcessFilter() override {};

    /** Standard PrintSelf method.*/
    virtual void PrintSelf(std::ostream& os, itk::Indent indent) const;

    void ExtractAndFilterSegmentsFromVectorDataBase(GDALDataset* vectorDataSrcDS,
            std::list<OGRLineString>& filteredSegments,
            std::list<OGRTriplet>& filteredTriplets);

    void ExtractAndFilterSegmentsFromLSD(GDALDataset* lsdfSrcDS, std::vector<OGRGeometry*> geometries,
            std::list<OGRLineString>& filteredSegments,
            std::list<OGRTriplet>& filteredTriplets);

    // Compute the best transformation
    std::vector<double> ComputeBestTransformation(const std::list<OGRTriplet>& imgTriplets, const std::list<OGRLineString>& imgSegments,
            const std::list<OGRTriplet>& vectorDBTriplets, const std::list<OGRLineString>& vectorDBSegments);

private:
    LSDToDBRegistrationProcessFilter(const Self&); //purposely not implemented
    void operator=(const Self&); //purposely not implemented

    std::string m_InputLSDFileName;
    std::string m_InputVectorDBName;
    std::string m_OutputDir;
    std::string m_OutputVectorDBName;
    OGRSpatialReference* m_spatialRef;

    // Length filtering
    DataType m_MinLengthThreshold;
    DataType m_MaxLengthThreshold;

    // Perpendicularity filtering
    DataType m_MaxDistancePerpendicularity;
    DataType m_AngleToleranceDeg; // in degree

    // Proximity filtering. Also used to compute padding
    // Also used as distance criteria for Registration
    DataType m_MaxDistanceToVectorDB;

    // For Testing purpose
    bool m_GenerateIntermediateData;
    std::string m_WorkingDir;
    std::string m_Extension;

    // OGR Filters
    OGRLineStringLengthFilter* m_LengthFilter;
    OGRLineStringProximityFilter* m_ProximityFilter;
    OGRLineStringPerpendicularityFilter* m_PerpendicularityFilter;
};

} // namespace otb

#include "otbLSDToDBRegistrationProcessFilter.txx"

#endif // NEW_IMAGE_TO_DB_REGISTRATION_PROCESS_FILTER_H
