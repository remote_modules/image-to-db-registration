/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef VECTOR_DATA_HELPER_H
#define VECTOR_DATA_HELPER_H

#include "itkObject.h"
#include "itkObjectFactory.h"

#include "otbVectorDataFileWriter.h"

namespace otb
{

/** \class VectorDataHelper
 *  \brief Provide methods to manipulate VectorData object.
 */

template <class TVectorData>
class ITK_EXPORT VectorDataHelper: public itk::Object
{
public:
  /** Standard class typedefs. */
  typedef VectorDataHelper   Self;
  typedef itk::Object Superclass;
  typedef itk::SmartPointer<Self>                 Pointer;
  typedef itk::SmartPointer<const Self>           ConstPointer;

  /** Run-time type information (and related methods). */
  itkTypeMacro(VectorDataHelper, itk::Object);

  /** Typdedef support for the VectroData*/
  typedef TVectorData                            VectorDataType;
  typedef typename VectorDataType::Pointer       VectorDataPointerType;
  typedef typename VectorDataType::DataNodeType  DataNodeType;
  typedef typename DataNodeType::Pointer         DataNodePointerType;
  typedef typename VectorDataType::DataTreeType::TreeNodeType InternalTreeNodeType;
  typedef typename VectorDataType::LineType      LineType;
  typedef typename VectorDataType::PointType     PointType;
  typedef typename LineType::VertexType          VertexType;
  typedef typename LineType::VertexListType      VertexListType;

  typedef std::vector<VertexType>                LineVectorType;
  typedef std::vector<LineVectorType>            MultiLineVectorType;

  typedef otb::VectorDataFileWriter<VectorDataType> VectorDataWriterType;

  // Convert an OGRGeometry to a LineVectorType.
  // Currently, the method only support Polygon, LineString and MultiLineString
  static void ConvertOGRGeometryToLineVector(const OGRGeometry* geometry, MultiLineVectorType& lineVector);

  // Compute the Euclidian distance between the two inputs vertex
  template<class T>
  static double ComputeEuclidianDistance(const T& currentVertex, const T& nextVertex);

  // Return the distance from one point to the input segment defined by [v0,v1]
  // The result is either the distance between the point and one segments extremities (strict = true)
  // or between the point to its orthogonal projection inside the segment (strict = false).
  template<class T>
  static double ComputeDistanceFromPointToSegment(const T& rAngle, const VertexType& v0, const VertexType& v1, bool strict);

  // Return the match score and match rate from the reference segments to their best match within the available lsd segments.
  // The match rate is the total length of the reference segments that have a match among the lsd segments.
  // The match score is the average distance (in meters) of the reference segments and the extemities of their match among the lsd segments. \
  // (cf. ComputeDistanceFromPointToSegment)
  // A reference segment is considereed to have is match if the distance to both lsd segment extremities are lower than match_threshold.
  static void ComputeDistanceFromSegmentsToSegments(const MultiLineVectorType& refLineVector, const MultiLineVectorType& lsdLineVector,
			const double& match_threshold, MultiLineVectorType& debugMetricsMatchsVector, double& final_score, double& final_rate);

  static void WriteVertexVectorToFile(const MultiLineVectorType& lineVector, std::string filename);

private:
  VectorDataHelper(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

};

} // namespace otb

#include "otbVectorDataHelper.txx"

#endif // VECTOR_DATA_HELPER_H
