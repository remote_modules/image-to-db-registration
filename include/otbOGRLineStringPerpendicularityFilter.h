/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OGR_LINESTRING_PERPENDICULARITY_FILTER_H
#define OGR_LINESTRING_PERPENDICULARITY_FILTER_H

#include "otbOGRLineStringFilter.h"
#include "otbOGRHelper.h"

namespace otb
{

class OGRLineStringPerpendicularityFilter : public OGRLineStringFilter
{
public:
    OGRLineStringPerpendicularityFilter(const double& maxDistancePerpendicularity, const double& angleToleranceDeg);

    // Filter the data by perpendicularity
    virtual void Filter();

    // Accessors
    void SetMaxDistancePerpendicularity(const double& maxDistancePerpendicularity) { m_MaxDistancePerpendicularity = maxDistancePerpendicularity; }
    double GetMaxDistancePerpendicularity() { return m_MaxDistancePerpendicularity; }
    void SetAngleToleranceDeg(const double& angleToleranceDeg);
    double GetAngleToleranceDeg() { return m_AngleToleranceDeg; }
    std::list<OGRTriplet> GetOutputTriplets() { return m_OutputTriplets; }

    virtual void ClearOutputs() {
        OGRLineStringFilter::ClearOutputs();
        m_OutputTriplets.clear();
    }

protected:
    // Compute the angle (in radian) between two line segments, assuming each line segments is composed of two points only
    double ComputeAngleBetweenLineSegments(const OGRLineString& lineSeg1, const OGRLineString& lineSeg2);

    // Compute the intersection point of two line segments, assuming each line segments is composed of two points only
    OGRPoint ComputeLineSegmentsIntersectionPoint(const OGRLineString& lineSeg1, const OGRLineString& lineSeg2);

    double m_MaxDistancePerpendicularity;
    double m_AngleToleranceDeg;
    double m_AngleToleranceRad;
    double m_AngleLow;
    double m_AngleHigh;
    std::list<OGRTriplet> m_OutputTriplets;
};


} // namespace otb

#endif
