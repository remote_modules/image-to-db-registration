/*
 * Copyright (C) 2005-2020 Centre National d'Etudes Spatiales (CNES)
 *
 * This file is part of Orfeo Toolbox
 *
 *     https://www.orfeo-toolbox.org/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "otbVectorDataHelper.h"
#include "otbOGRIOHelper.h"
#include "otbMath.h"
#include <algorithm>

namespace otb
{
// Convert an OGRGeometry to a LineVectorType.
// Currently, the method only support Polygon, LineString and MultiLineString
template <class TVectorData>
void
VectorDataHelper<TVectorData>
::ConvertOGRGeometryToLineVector(const OGRGeometry* geometry, MultiLineVectorType& lineVector) {
    otb::OGRIOHelper::Pointer OGRConversion = otb::OGRIOHelper::New();
    switch (geometry->getGeometryType()) {
        case wkbPolygon:
        {
            DataNodePointerType dataNode = DataNodeType::New();
            OGRConversion->ConvertGeometryToPolygonNode(geometry, dataNode);
            if(dataNode->IsPolygonFeature()) {
                typename VertexListType::Pointer vlist = dataNode->GetPolygonExteriorRing()->GetVertexList();
                if (vlist->Size() > 0) {
                    typename VertexListType::ConstIterator it = vlist->Begin();
                    VertexType currentVertex = it.Value();
                    ++it;
                    while (it != vlist->End()) {
                        VertexType nextVertex = it.Value();
                        LineVectorType line;
                        line.push_back(currentVertex);
                        line.push_back(nextVertex);

                        /** add line to input parameter lineVector */
                        lineVector.push_back(line);

                        currentVertex = nextVertex;
                        ++it;
                    }
                }
            }
            break;
        }
        case wkbLineString:
        {
            DataNodePointerType dataNode = DataNodeType::New();
            OGRConversion->ConvertGeometryToLineNode(geometry, dataNode);
            if(dataNode->IsLineFeature()) {
                typename VertexListType::Pointer vlist = dataNode->GetLine()->GetVertexList();
                if (vlist->Size() > 0) {
                    typename VertexListType::ConstIterator it = vlist->Begin();
                    VertexType currentVertex = it.Value();
                    ++it;
                    while (it != vlist->End()) {
                        VertexType nextVertex = it.Value();
                        LineVectorType line;
                        line.push_back(currentVertex);
                        line.push_back(nextVertex);

                        /** add line to input parameter lineVector */
                        lineVector.push_back(line);

                        currentVertex = nextVertex;
                        ++it;
                    }
                }
            }
            break;
        }
        case wkbMultiLineString:
        {
            OGRMultiLineString * ogrMulti = (OGRMultiLineString *) geometry;
            for (int geoIndex = 0; geoIndex < ogrMulti->getNumGeometries(); ++geoIndex) {
                DataNodePointerType dataNode = DataNodeType::New();
                OGRConversion->ConvertGeometryToLineNode(ogrMulti->getGeometryRef(geoIndex), dataNode);
                if(dataNode->IsLineFeature()) {
                    typename VertexListType::Pointer vlist = dataNode->GetLine()->GetVertexList();
                    if (vlist->Size() > 0) {
                        typename VertexListType::ConstIterator it = vlist->Begin();
                        VertexType currentVertex = it.Value();
                        ++it;
                        while (it != vlist->End()) {
                            VertexType nextVertex = it.Value();
                            LineVectorType line;
                            line.push_back(currentVertex);
                            line.push_back(nextVertex);

                            /** add line to input parameter lineVector */
                            lineVector.push_back(line);

                            currentVertex = nextVertex;
                            ++it;
                        }
                    }
                }
            }
            break;
        }
    }
}

// Compute the Euclidian distance between the two inputs vertex
template <class TVectorData> template <class T>
double
VectorDataHelper<TVectorData>
::ComputeEuclidianDistance(const T &currentVertex, const T &nextVertex) {
	return vcl_sqrt((currentVertex[0]-nextVertex[0])*(currentVertex[0]-nextVertex[0])+(currentVertex[1]-nextVertex[1])*(currentVertex[1]-nextVertex[1]));
}

// Return the distance from one point to the input segment defined by [v0,v1]
// The result is either the distance between the point and one segments extremities (strict = true)
// or between the point to its orthogonal projection inside the segment (strict = false).
template <class TVectorData> template <class T>
double
VectorDataHelper<TVectorData>
::ComputeDistanceFromPointToSegment(const T& rAngle, const VertexType& v0, const VertexType& v1, bool strict) {
	// Case the extremities of the segment are the same
	double l2 = v0.SquaredEuclideanDistanceTo(v1);

	// Is the projection of rAngle on the segment inside (0<u<1) or
	// inside the segment bounds
	double u = ((rAngle[0] - v0[0]) *(v1[0] - v0[0] ) + (rAngle[1] - v0[1]) *(v1[1] - v0[1])) / l2;

	if (strict) {
		if( u <= 0.5 ) u = 0.;
		if( u > 0.5 ) u = 1.;
	} else {
		if( u <= 0 ) u = 0.;
		if( u > 1 ) u = 1.;
	}

	double x = v0[0] + u *(v1[0] - v0[0] );
	double y = v0[1] + u *(v1[1] - v0[1] );

	double dx = x - rAngle[0];
	double dy = y - rAngle[1];

	return vcl_sqrt(dx*dx + dy*dy);
}

// Return the match score and match rate from the reference segments to their best match within the available LSD segments.
// The match rate is the total length of the reference segments that have a match among the LSD segments.
// The match score is the average distance (in meters) of the reference segments and the extremities of their match among the LSD segments. \
// (cf. ComputeDistanceFromPointToSegment)
// A reference segment is considered to have is match if the distance to both LSD segment extremities are lower than match_threshold.
template <class TVectorData>
void
VectorDataHelper<TVectorData>
::ComputeDistanceFromSegmentsToSegments(const MultiLineVectorType& refLineVector, const MultiLineVectorType& lsdLineVector,
		const double& match_threshold, MultiLineVectorType& debugMetricsMatchsVector, double& final_score, double& final_rate) {
	// Start actual statistic computation
	// refLineVector is the vector containing the sub-segments of the database current feature
	typename MultiLineVectorType::const_iterator itSecond    = refLineVector.begin();
	typename MultiLineVectorType::const_iterator itEndSecond = refLineVector.end();

	double inliers_count = 0;
	double inliers_length = 0;
	double total = 0;
	double c_dist = 0;
	while(itSecond != itEndSecond) {
		// lsdLineVector is the vector containing the LSD segments found in the neighbourhood of the database current feature
		typename MultiLineVectorType::const_iterator itFirst    = lsdLineVector.begin();
		typename MultiLineVectorType::const_iterator itEndFirst = lsdLineVector.end();

		double length = ComputeEuclidianDistance((*itSecond)[0], (*itSecond)[1]);
		double inlier_dist = 0;
		double min_inlier_dist = match_threshold;
		typename MultiLineVectorType::const_iterator itMatch;
		while(itFirst != itEndFirst ) {
			// Compute distance of the LSD segment extremities to the database feature sub-segments
			double dist1 = ComputeDistanceFromPointToSegment((*itFirst)[0], (*itSecond)[0], (*itSecond)[1], false);
			double dist2 = ComputeDistanceFromPointToSegment((*itFirst)[1], (*itSecond)[0], (*itSecond)[1], false);
			inlier_dist = (dist1 + dist2) / 2;

			if( (inlier_dist < min_inlier_dist) && (dist1 < match_threshold) && (dist2 < match_threshold) ) {
				min_inlier_dist = inlier_dist;
				itMatch = itFirst;
			}
			++itFirst;
		}

		// The database feature sub-segment has a match if the LSD segment distance is below the required threshold
		if( min_inlier_dist < match_threshold) {
			c_dist += min_inlier_dist;
			inliers_length += length;
			++inliers_count;
			debugMetricsMatchsVector.push_back(*itMatch);
		}
		total += length;
		++itSecond;
	}

	// Compute feature final match rate and final score
	if(inliers_count) {
		// The score of the database feature is the average of the distances of its sub-segments that has a match with an LSD segment.
		final_score = c_dist/inliers_count;
	}

	if(total !=0) {
		final_rate = (double)inliers_length/total;
	}
}

template <class TVectorData>
void
VectorDataHelper<TVectorData>
::WriteVertexVectorToFile(const MultiLineVectorType& lineVector, std::string filename) {
    //Preparing the output vectordata
    VectorDataPointerType debugVectorData = VectorDataType::New();
    typename VectorDataType::DataTreePointerType otree = debugVectorData->GetDataTree();
    InternalTreeNodeType* outputRoot = const_cast<InternalTreeNodeType *>(otree->GetRoot());

    //add the root
    typename DataNodeType::Pointer document = DataNodeType::New();
    typename DataNodeType::Pointer folder   = DataNodeType::New();
    typename DataNodeType::Pointer root     = outputRoot->Get();

    //add a document
    document->SetNodeType(otb::DOCUMENT);
    document->SetNodeId("DOCUMENT");

    //add a  folder
    folder->SetNodeType(otb::FOLDER);
    folder->SetNodeId("FOLDER");

    //Build the output tree
    otree->Add(document, root);
    otree->Add(folder, document);

    // Iterate through the match segments.
    typename MultiLineVectorType::const_iterator it1 = lineVector.begin();
    while(it1 != lineVector.end()) {
        DataNodePointerType line1 = DataNodeType::New();
        line1->SetNodeType(otb::FEATURE_LINE);
        line1->SetNodeId("FEATURE_LINE");
        typename DataNodeType::LineType::Pointer l1 = DataNodeType::LineType::New();
        l1->AddVertex((*it1)[0]);
        l1->AddVertex((*it1)[1]);
        line1->SetLine(l1);
        otree->Add(line1, folder);

        ++it1;
    }

    typename VectorDataWriterType::Pointer debug_writer = VectorDataWriterType::New();
    debug_writer->SetInput(debugVectorData);
    debug_writer->SetFileName(filename);
    debug_writer->Update();
}

} // namespace otb
