# ImageToDBRegistration
## Synopsis

This code implements the algorithm for automated vector Database registration onto an Image.  
This first reproject the vector database onto image geometry. Then, it croppes the image around the reprojected database in order to process Line Segment Detection (LSD)
algorithm. The chosen LSD algorithm is the default one provided by OTB framework, or the one implemented by CMLA, available in lsd-cmla-remote-module as
`FastLineSegmentDetection` application. Then the LSD is filtered and the vector database is registered. Finally, evaluation metrics are computed on the registered database.

## Usage

Run the python script ImageToDBRegistration.py with the following arguments:

```
usage: ImageToDBRegistration.py [-h] [-margin MARGIN] [-flsd FLSD]
                                [-scale SCALE] [-sigmacoef SIGMACOEF]
                                [-quant QUANT] [-angth ANGTH] [-logeps LOGEPS]
                                [-densityth DENSITYTH] [-nbins NBINS]
                                [-tilesizex TILESIZEX] [-tilesizey TILESIZEY]
                                [-minlenth MINLENTH] [-maxlenth MAXLENTH]
                                [-maxdistperp MAXDISTPERP]
                                [-angletol ANGLETOL] [-maxdistdb MAXDISTDB]
                                [-matchth MATCHTH] [-rateth RATETH]
                                [-clean CLEAN]
                                im db wd

positional arguments:
  im                    Input image
  db                    Input db
  wd                    Working directory

optional arguments:
  -h, --help            show this help message and exit
  -margin MARGIN        Margin applied on the global vector database's
                        envelope (default : 10.)
  -flsd FLSD            Use fast lsd method (if not set, default lsd process
                        will be used)
  -scale SCALE          Gaussian scaling (default : 0.8)
  -sigmacoef SIGMACOEF  Ratio for sigma of gaussian filter (default : 0.6)
  -quant QUANT          Gradient quantization error (default : 2.)
  -angth ANGTH          Gradient angle tolerance (default : 22.5)
  -logeps LOGEPS        Detection threshold (default : 0.)
  -densityth DENSITYTH  Minimal density of region points in a rectangle to be
                        accepted (default : 0.7)
  -nbins NBINS          Number of bins in ordering (default : 1024)
  -tilesizex TILESIZEX  Size of tiles in pixel along X-axis (default : 500)
  -tilesizey TILESIZEY  Size of tiles in pixel along Y-axis (default : 500)
  -minlenth MINLENTH    Minimum threshold for length filter (default : 5.)
  -maxlenth MAXLENTH    Maximum threshold for length filter (default : 100.)
  -maxdistperp MAXDISTPERP
                        Max distance allowed between 2 perpendicular segments
                        (default : 10.)
  -angletol ANGLETOL    Angle tolerance (in degree) for perpendicular filter
                        (default : 15.)
  -maxdistdb MAXDISTDB  Maximum distance to any geometry of the vector
                        database allowed (default : 10.)
  -matchth MATCHTH      Match threshold (default : 2.0)
  -rateth RATETH        Rate threshold (default : 0.3)
  -clean CLEAN          Clean intermediate outputs (default : True)
```

## Installation

ImageToDBRegistration.py relies on specific OTB applications and uses CMake (http://www.cmake.org) for building from source.

## Dependencies

Following a summary of the required dependencies: 

* GDAL >= 3.1
* OTB >= 7.2
* Python interpreter >= 3.7.2
* Python libs >= 3.7.2

GDAL itself depends on a number of other libraries provided by most major operating systems and also depends on the non standard GEOS and Proj libraries. GDAL- Python bindings are also required

Python package dependencies:

* sys
* subprocess
* glob
* os
* gdal
* logging

Other dependencies:
The lsd-cmla-remote-module must be installed as an OTB application.
https://gitlab.cnes.fr/tanguyy/lsd-cmla-remote-module

*Note:* If you want to use the provided applications with a GDAL version < 3.1, you may need to update the file `include/otbOGRAffineTransformation.h`.

## Tests

Enable tests with BUILD_TESTING cmake option. Use `ctest` command to run tests. Do not forget to clean your output test directory when you run a new set of tests.

For test purpose, the variable `TEST_DATA_ROOT` must be set within cmake.

A dataset archive is available [here](https://gitlab.orfeo-toolbox.org/remote_modules/image-to-db-registration/-/wikis/Test-data-for-the-remote-module)

## Contributors

Yannick Tanguy (CNES), Rémi Demortier (Thales), Germain Salgues (Magellium)
