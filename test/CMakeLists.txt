otb_module_test()

#${otb-module} will be the name of this module and will not need to be
#changed when this module is renamed.

set(TEST_DATA_ROOT CACHE PATH "The test data directory which contains 2 sub-folders (Baseline/Input-Data-Test)" )
mark_as_advanced(TEST_DATA_ROOT)
if(NOT TEST_DATA_ROOT)
  message(FATAL_ERROR "Please set TEST_DATA_ROOT")
endif()

set(BASELINE ${TEST_DATA_ROOT}/Baseline)
set(DATA_TEST ${TEST_DATA_ROOT}/Input-Data-Test)
set(OUTPUT_TEST ${CMAKE_BINARY_DIR}/Testing/Temporary)

find_package(PythonInterp REQUIRED)
find_package(PythonLibs 3.7 REQUIRED)

# Cropping image around DB application
otb_test_application(
  NAME otbCropAroundDB
  APP  CropAroundDB
  OPTIONS
    -im ${DATA_TEST}/base_image.TIF
    -db ${DATA_TEST}/database_reprojected.sqlite
    -out ${OUTPUT_TEST}/cropped_image.TIF
  VALID --compare-image 0.0001
    ${BASELINE}/ref_cropped_image.TIF
    ${OUTPUT_TEST}/cropped_image.TIF
)

# LSD filtration and registration application
otb_test_application(
  NAME otbLSDToDBRegistration
  APP  LSDToDBRegistration
  OPTIONS
    -lsd ${DATA_TEST}/lsd_fast.sqlite
    -db ${DATA_TEST}/database_reprojected.sqlite
    -out database_registered.sqlite
    -outputdir ${OUTPUT_TEST}
  VALID --compare-ogr 0.0001
    ${BASELINE}/ref_database_registered.sqlite
    ${OUTPUT_TEST}/database_registered.sqlite
)

# Evaluation application
otb_test_application(
  NAME otbVectorDataEvaluation
  APP  VectorDataEvaluation
  OPTIONS
    -in ${DATA_TEST}/lsd_fast.sqlite
    -ref ${DATA_TEST}/database_registered.sqlite
    -outstats ${OUTPUT_TEST}/globalstats.xml
  VALID --compare-ascii 0.0
    ${BASELINE}/ref_globalstats.xml
    ${OUTPUT_TEST}/globalstats.xml
)
