cmake_minimum_required(VERSION 2.8.9)
PROJECT(ImageToDBRegistration)

FIND_PACKAGE(Boost REQUIRED system serialization)

# This module create a library, with the same name as the module
SET(ImageToDBRegistration_LIBRARIES ImageToDBRegistration)

IF(NOT OTB_SOURCE_DIR)
  # Handle the compilation outside OTB source tree: find where OTB is built/installed
  FIND_PACKAGE(OTB REQUIRED)
  LIST(APPEND CMAKE_MODULE_PATH ${OTB_CMAKE_DIR})
  
  # This script will process the module sources
  INCLUDE(OTBModuleExternal)
ELSE()
  otb_module_impl()
ENDIF()

